# Mod Yu-Gi-Oh! Forbidden Memories para Play Station 1

Licença:

1. Livre não comercial sem retirar os créditos;
2. Algumas dependências são de uso não comercial.

Considerações:

1. Desenvolvimento na versão do Delphi 7.

Dependências:

1. Alpha Controls;
2. DevExpress;
3. GIFImage.

* Todas as dependências são de utilização opcional.
 - Para remover as depências, primeiro deve renomear os arquivos DFM.

Rodando os testes

1. Endereços de memória encontrados através do aplicativo Cheat Engine;
2. Emulador usado nos testes: epSXe 1.9

Outros

1. FreeUPX - Para compactação dos recursos;
2. Modo Administração para melhor desempenho.

Jogo

1. Abra o diretório .\EXECUTAR\YuGiOh APP Mod.exe e veja o tutorial para melhor explicações.
* O tutorial pode não coresponder todas informações, principalmente por sofrer melhorias ou alterações no código.