unit Unit2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sCheckBox, sGroupBox, Buttons, sBitBtn, sComboBox,
  sLabel, ExtCtrls, sPanel, sEdit, sSpinEdit, ComCtrls, sListView, Unit1,
  sMemo, sRadioButton, sSpeedButton, sBevel, sTrackBar, sButton;

type
  TForm2 = class(TForm)
    sGroupBox2: TsGroupBox;
    rgMonstros: TsRadioGroup;
    rgOutros: TsRadioGroup;
    sPanelBTNs: TsPanel;
    btSave: TsBitBtn;
    btCancel: TsBitBtn;
    sGroupBox3: TsGroupBox;
    lbAvancDescInterval: TsLabel;
    spAvancInterval: TsSpinEdit;
    rgAlpha: TsRadioGroup;
    sRadioGroupReliquia: TsRadioGroup;
    sGroupMais: TsGroupBox;
    sCheckReceivStar: TsCheckBox;
    sCheckPassInexistente: TsCheckBox;
    sCheckPreviewRank: TsCheckBox;
    sCheckIlimitedCard: TsCheckBox;
    sCheckReliquia: TsCheckBox;
    sCheckDropMOD: TsCheckBox;
    sCheckModDef: TsCheckBox;
    sCheckOpaque: TsCheckBox;
    sPanelRecomend: TsPanel;
    sbtnShowHideSetCFG: TsSpeedButton;
    sRadioSetCfg1: TsRadioButton;
    sRadioSetCfg2: TsRadioButton;
    sRadioSetCfg3: TsRadioButton;
    sRadioSetCfg4: TsRadioButton;
    sBevel1: TsBevel;
    sLabelsSetCfg1: TsLabel;
    sLabelsSetCfg3: TsLabel;
    sLabelsSetCfg2: TsLabel;
    sLabelsSetCfg4: TsLabel;
    sCheckHotKeys: TsCheckBox;
    sRadioSkin1: TsRadioButton;
    sRadioSkin2: TsRadioButton;
    sLabelSkin: TsLabel;
    sRadioSkin3: TsRadioButton;
    sRadioSkin4: TsRadioButton;
    sPanel1: TsPanel;
    btnEscalaAM: TsButton;
    sCheckPeB: TsCheckBox;
    sCheckReliquiaMove: TsCheckBox;
    sCheckEscala: TsCheckBox;
    btnEscalaRD: TsButton;
    lblMsgEscala: TLabel;
    procedure btCancelClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btSaveClick(Sender: TObject);
    procedure sRadioSetCfg1Click(Sender: TObject);
    procedure sbtnShowHideSetCFGClick(Sender: TObject);
    procedure btnEscalaAMClick(Sender: TObject);
    procedure sCheckEscalaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

uses StrUtils;

{$R *.dfm}

procedure TForm2.btCancelClick(Sender: TObject);
begin
Close;
end;

procedure TForm2.FormShow(Sender: TObject);
begin
 rgMonstros.ItemIndex := Config.IsViewMonster;
 rgOutros.ItemIndex := Config.IsViewOther;
 rgAlpha.ItemIndex := Config.IsViewAlpha;
 spAvancInterval.Value := Config.IsInterval;
 sCheckReceivStar.Checked := Config.IsStarReceiv;
 sCheckIlimitedCard.Checked := Config.IsNotLimitedCard;
 sCheckPassInexistente.Checked := Config.IsYesPassword;
 sCheckPreviewRank.Checked := Config.IsRankPreview;
 sCheckReliquia.Checked := Config.IsReliquiasOn;
 sRadioGroupReliquia.ItemIndex := Config.IsReliquiasGet;
 sCheckReliquiaMove.Checked := Config.IsReliquiaGifMove;
 sCheckDropMOD.Checked := Config.isDropMOD;
 sCheckModDef.Checked := Config.IsModDEF;
 sCheckOpaque.Checked := Config.IsOpaque;
 sCheckPeB.Checked := Config.IsPeB;
 sCheckHotKeys.Checked := Config.wHotKeys;
 sCheckEscala.Checked := Config.escala;
 btnEscalaAM.Enabled := Config.escalaPos <= 10;
 btnEscalaRD.Enabled := Config.escalaPos >= -5;
 if Config.wSkin = 255 then
    sRadioSkin4.Checked := True else
 if Config.wSkin = 1 then
    sRadioSkin2.Checked := True else
 if Config.wSkin = 2 then
    sRadioSkin3.Checked := True else
    sRadioSkin1.Checked := True;
 DoubleBuffered := False;
end;

procedure TForm2.btSaveClick(Sender: TObject);
begin
 Config.IsViewMonster := rgMonstros.ItemIndex;
 Config.IsModDEF := sCheckModDef.Checked;
 Config.IsViewOther := rgOutros.ItemIndex;
 Config.IsViewAlpha := rgAlpha.ItemIndex;
 Config.IsInterval := spAvancInterval.Value;
 Config.IsStarReceiv := sCheckReceivStar.Checked;
 Config.IsNotLimitedCard := sCheckIlimitedCard.Checked;
 Config.IsYesPassword := sCheckPassInexistente.Checked;
 Config.IsRankPreview := sCheckPreviewRank.Checked;
 Config.IsReliquiasOn :=  sCheckReliquia.Checked; 
 Config.IsReliquiasGet := sRadioGroupReliquia.ItemIndex;
 Config.IsReliquiaGifMove := sCheckReliquiaMove.Checked;
 Config.isDropMOD := sCheckDropMOD.Checked;
 Config.IsOpaque := sCheckOpaque.Checked;
 Config.IsPeB := sCheckPeB.Checked;
 Config.wHotKeys := sCheckHotKeys.Checked;
 if sRadioSkin4.Checked then
    Config.wSkin := 255 else
 if sRadioSkin3.Checked then
    Config.wSkin := 2 else
 if sRadioSkin2.Checked then
    Config.wSkin := 1 else
    Config.wSkin := 0;
 Form1.salvarConfig;
 Form1.executarConfig;
 Form1.lbReduze.OnClick(Form1.lbReduze);
 Form1.lbReduze.OnClick(Form1.lbReduze);
 Close;
end;

procedure TForm2.sRadioSetCfg1Click(Sender: TObject);
begin
 case AnsiIndexText(TsRadioButton(Sender).Name,
  ['sRadioSetCfg1','sRadioSetCfg2','sRadioSetCfg3','sRadioSetCfg4']) of
   0: begin
       rgMonstros.ItemIndex := 1;
       rgOutros.ItemIndex := 1;
       sCheckOpaque.Checked := False;
       sCheckPeB.Checked := True;
       sCheckReliquiaMove.Checked := False;
       rgAlpha.ItemIndex := 0;
       sCheckModDef.Checked := False;
       spAvancInterval.Value := 1000;
       sCheckPreviewRank.Checked := False;
       sRadioSkin4.Checked := True;
      end;
   1: begin
       rgMonstros.ItemIndex := 1;
       rgOutros.ItemIndex := 0;
       sCheckOpaque.Checked := False;
       sCheckPeB.Checked := False;
       sCheckReliquiaMove.Checked := False;
       rgAlpha.ItemIndex := 1;
       sCheckModDef.Checked := False;
       spAvancInterval.Value := 100;
       sCheckPreviewRank.Checked := True;
      end;
   2: begin
       rgMonstros.ItemIndex := 1;
       rgOutros.ItemIndex := 2;
       sCheckOpaque.Checked := False;
       sCheckPeB.Checked := False;
       sCheckReliquiaMove.Checked := True;
       rgAlpha.ItemIndex := 1;
       sCheckModDef.Checked := False;
       spAvancInterval.Value := 150;
       sCheckPreviewRank.Checked := True;
      end;
   3: begin
       rgMonstros.ItemIndex := 2;
       rgOutros.ItemIndex := 2;
       sCheckOpaque.Checked := True;
       sCheckPeB.Checked := False;
       sCheckReliquiaMove.Checked := True;
       rgAlpha.ItemIndex := 0;
       sCheckModDef.Checked := True;
       spAvancInterval.Value := 200;
       sCheckPreviewRank.Checked := True;
      end;
 end;
end;

procedure TForm2.sbtnShowHideSetCFGClick(Sender: TObject);
begin
 sPanelRecomend.Visible := not sPanelRecomend.Visible;
 sPanelRecomend.SkinData.SkinSection := 'PANEL';
 sPanelRecomend.SkinData.SkinSection := 'GAUGE';
end;

procedure TForm2.btnEscalaAMClick(Sender: TObject);
begin
  if (Sender is TsButton) then
  if (Sender as TsButton).Name = 'btnEscalaRD' then
  begin
   Config.escalaPos := Config.escalaPos - 1;
   Form1.ScaleForm(Form1, -5);
  end else
  begin
   Config.escalaPos := Config.escalaPos + 1;
   Form1.ScaleForm(Form1, 5);
  end;
   btnEscalaAM.Enabled := Config.escalaPos <= 10;
   btnEscalaRD.Enabled := Config.escalaPos >= -5;
end;

procedure TForm2.sCheckEscalaClick(Sender: TObject);
begin
  if (not sCheckEscala.Checked) then
  begin
    while Config.escalaPos <> 0 do
    begin
      if (Config.escalaPos <= 0) then
      begin
        Config.escalaPos := Config.escalaPos + 1;
        if (Config.escalaPos <> 0) then
            Form1.ScaleForm(Form1, 5);
      end else
      begin
        Config.escalaPos := Config.escalaPos - 1;
        if (Config.escalaPos <> 0) then
            Form1.ScaleForm(Form1, -5);
      end;
    end;
    btnEscalaAM.Enabled := True;
    btnEscalaRD.Enabled := True;
  end;

  Config.escala := sCheckEscala.Checked;
end;

end.
