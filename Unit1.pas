unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, sSkinProvider, sSkinManager, ExtCtrls, sPanel, Buttons, IniFiles,
  StdCtrls, sComboBox, sBitBtn, sLabel, sSpeedButton, Tlhelp32, Math,
  jpeg, ComCtrls, sStatusBar, ImgList, acPNG, cxClasses, cxGraphics,
  dxGDIPlusClasses, cxListView, acAlphaImageList, mmsystem, sCheckBox,
  sBevel, sEdit, sSpinEdit, sGauge, GIFImage;

type
  fConfig = record
  IsViewMonster: Byte;
  IsViewOther: Byte;
  IsViewAlpha: Byte;
  IsInterval: Integer;
  IsStarReceiv: Boolean;
  IsNotLimitedCard: Boolean;
  IsYesPassword: Boolean;
  IsRankPreview: Boolean;
  IsReliquiasOn: Boolean;
  IsReliquiasGet: Byte;
  IsReliquiaGifMove: Boolean;
  isDropMOD: Boolean;
  IsModDEF: Boolean;
  IsOpaque: Boolean;
  IsPeB: Boolean;
  IsDebug77: Boolean;
  wHotKeys: Boolean;
  wSkin: Byte;
  wEmu: Shortint;
  wMax: Boolean;
  wDBMax: Boolean;
  wDBImgShow: Boolean;
  wPosX: Integer;
  wPosY: Integer;
  escalaPos: Integer;
  escala: Boolean;
  end;

type
  fCodes = record
  Is1: string;
  Is2: string;
  Is3: string;
  Is4: string;
  Is5: string;
  Is6: string;
  Is7: string;
  Is8: string;
  Is9: string;
  Is10: string;
  end;

type
  fReliquia = record
  ID: Word;
  sAT: Boolean;
  sATc: Byte;
  ATK: Word;
  DEF: Word;
  ATKG: Word;
  DEFG: Word;
  sTurn: Byte;
  sTurnE: Byte;
  sTurnB: Boolean;
  TurnF: Byte;
  Tipo: Byte;
  Tp_Card: Byte;
  Tp_Monst: Word;
  end;

type
  TForm1 = class(TForm)
    sSkinManager1: TsSkinManager;
    sSkinProvider1: TsSkinProvider;
    TIProcess: TTimer;
    sPanelBG: TsPanel;
    sPanelC: TsPanel;
    img1: TImage;
    btAT: TsSpeedButton;
    btCfg: TsSpeedButton;
    lbClose: TsWebLabel;
    blMinimize: TsWebLabel;
    sPanelM: TsPanel;
    img01: TImage;
    img02: TImage;
    img03: TImage;
    img04: TImage;
    img05: TImage;
    shp2: TShape;
    shp3: TShape;
    shp4: TShape;
    shp5: TShape;
    shp6: TShape;
    lbID3: TsLabel;
    lbATK3: TsLabel;
    lbDEF3: TsLabel;
    lbID2: TsLabel;
    lbATK2: TsLabel;
    lbDEF2: TsLabel;
    lbDEF1: TsLabel;
    lbATK1: TsLabel;
    lbID5: TsLabel;
    lbATK5: TsLabel;
    lbDEF5: TsLabel;
    lbID4: TsLabel;
    lbATK4: TsLabel;
    lbDEF4: TsLabel;
    lbID1: TsLabel;
    lbSymbol5: TsLabel;
    lbSymbol4: TsLabel;
    lbSymbol3: TsLabel;
    lbSymbol2: TsLabel;
    lbSymbol1: TsLabel;
    sPanelO: TsPanel;
    img06: TImage;
    img07: TImage;
    img08: TImage;
    img09: TImage;
    img10: TImage;
    shp7: TShape;
    shp8: TShape;
    shp9: TShape;
    shp10: TShape;
    shp11: TShape;
    lbID10: TsLabel;
    lbID9: TsLabel;
    lbID8: TsLabel;
    lbID7: TsLabel;
    lbID6: TsLabel;
    lbTP10: TsLabel;
    lbTP6: TsLabel;
    lbTP7: TsLabel;
    lbTP8: TsLabel;
    lbTP9: TsLabel;
    btDB: TsSpeedButton;
    lbReduze: TsWebLabel;
    lbTurno: TsLabel;
    sPanelF: TsPanel;
    lbDescBiome: TsLabel;
    sStatusBar1: TsStatusBar;
    imgNPC: TImage;
    ilImgNPC: TImageList;
    ilImgBiome: TImageList;
    btnInfo: TsSpeedButton;
    cxImgColecaoAtivado: TcxImageCollection;
    cxImgColecaoItem1: TcxImageCollectionItem;
    cxImgColecaoItem2: TcxImageCollectionItem;
    cxImgColecaoItem3: TcxImageCollectionItem;
    sIconList: TsAlphaImageList;
    cxImgColecaoItem4: TcxImageCollectionItem;
    cxImgColecaoItem5: TcxImageCollectionItem;
    sPanelRun: TsPanel;
    lbDescRun: TsLabel;
    sWebLabel1: TsWebLabel;
    sWebLabel2: TsWebLabel;
    sLabelDescObsEmu: TsLabel;
    sLabelRunEmu: TsLabel;
    sWebLabel5: TsWebLabel;
    sWebLabel3: TsWebLabel;
    btRun: TsBitBtn;
    cbProcessos: TsComboBox;
    sCheckEmu: TsCheckBox;
    cbEmulador: TsComboBox;
    pnlDebug: TsPanel;
    sPaneDeb: TsPanel;
    sBevelSeparaYouCPU: TsBevel;
    lblDescYou: TLabel;
    lblDescCPU: TLabel;
    lblDescLP: TLabel;
    lblDescDeck: TLabel;
    sSpeedButtonOkEdit: TsSpeedButton;
    slbNotVisiblePaneDEB: TsWebLabel;
    btnDebugAllCards: TsSpeedButton;
    btnDebugAllCpus: TsSpeedButton;
    btnDebugAllStars: TsSpeedButton;
    btnDebugDuelsMod: TsSpeedButton;
    btnDebugDuelsEnd: TsSpeedButton;
    btnDebugInfo: TsSpeedButton;
    sCheckLPYou: TsCheckBox;
    sCheckDeckYou: TsCheckBox;
    sSpinEditLPorDeck: TsSpinEdit;
    sCheckLPCPU: TsCheckBox;
    sCheckDeckCPU: TsCheckBox;
    cxImgColecaoDesativado: TcxImageCollection;
    cxImageCollectionItem1: TcxImageCollectionItem;
    cxImageCollectionItem2: TcxImageCollectionItem;
    cxImageCollectionItem3: TcxImageCollectionItem;
    cxImageCollectionItem4: TcxImageCollectionItem;
    cxImageCollectionItem5: TcxImageCollectionItem;
    cxImgColecaoAtivadoGif: TcxImageCollection;
    cxImageCollectionItem6: TcxImageCollectionItem;
    cxImageCollectionItem7: TcxImageCollectionItem;
    cxImageCollectionItem8: TcxImageCollectionItem;
    cxImageCollectionItem9: TcxImageCollectionItem;
    cxImageCollectionItem10: TcxImageCollectionItem;
    imgBiome: TImage;
    procedure img1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lbCloseClick(Sender: TObject);
    procedure btCfgClick(Sender: TObject);
    procedure btATClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure sWebLabel1Click(Sender: TObject);
    procedure sWebLabel2Click(Sender: TObject);
    procedure TIProcessTimer(Sender: TObject);
    procedure btRunClick(Sender: TObject);
    procedure blMinimizeClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btDBClick(Sender: TObject);
    procedure btnInfoClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure lbReduzeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure sCheckEmuClick(Sender: TObject);
    procedure cbProcessosChange(Sender: TObject);
    procedure cbProcessosClick(Sender: TObject);
    procedure sWebLabel5Click(Sender: TObject);
    procedure sWebLabel3Click(Sender: TObject);
    procedure lbDescBiomeClick(Sender: TObject);
    procedure sSpeedButtonOkEditClick(Sender: TObject);
    procedure sCheckLPYouMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure sSpinEditLPorDeckExit(Sender: TObject);
    procedure slbNotVisiblePaneDEBClick(Sender: TObject);
    procedure sSpinEditLPorDeckKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnDebugInfoClick(Sender: TObject);
    procedure btnDebugAllCardsClick(Sender: TObject);
    procedure btnDebugDuelsModClick(Sender: TObject);
    procedure lbDescBiomeMouseEnter(Sender: TObject);
    procedure lbDescBiomeMouseLeave(Sender: TObject);
    procedure imgNPCClick(Sender: TObject);
  private
    { Private declarations }
    procedure WMHotKey(var Msg: TWMHotKey); message WM_HOTKEY;
  protected
    procedure CreateParams(var Params: TCreateParams); override;
  public
    { Public declarations }
    procedure verificarEmulador;
    function GetEmule(Endereco: Variant): Integer;
    procedure visualizarImagem(ID: Integer; var IMG: TImage; const PeB: Boolean = False; const qualidade: Integer = 100);
  //  procedure LerMemoria(Processo: THandle; Endereco: String; var Valor: LongInt; QntByte: Byte);
  //  procedure EscreverMemoria(Processo: THandle; Endereco: String; var Valor: LongInt; QntByte: Byte);
    procedure ScaleForm(F: TForm; Tamanho: Integer);
    function UnderWine: Boolean;
    class procedure reproduzirAudio(const tipo: Integer = 0);
    procedure OpacidadeIMG(Bmp: TBitmap; Count: Byte);
    procedure ListarProcessosAbertos;
    procedure salvarConfig(const auto: Boolean = False);
    procedure lerConfig;
    procedure executarConfig;
    procedure abrirImagem(ID: Integer; var IMG: TImage; IsAlpha: Boolean; const isDEF: Byte = 0; const inUse: Integer = 0);
    function verificarSmallInt(X: Integer):Integer;
    procedure selecionarBioma(X: Integer);
    procedure alterarInfoATKDEF(isCard,ATK,DEF,EQP,BIO: Smallint; var lbATK, lbDEF: TsLabel);
    procedure alterarSimbolo(ID: Word; var lbSimbol: TsLabel);
    procedure alterarTipo(ID: Word; var lbTipo: TsLabel);
    Procedure restaurarDescricoes;
    function calcularResultado(MyLP: Word; Vitory, Turn, EffectATK, DefWin, DownFace, Fusoes, Equips, Magias, Traps, Combo, UseCards:Byte): String;
    function calcular(hProcess: THandle): String;
    function Dropar(NPC: Byte; qntDuel: Word; Rank: string; var Card: Word): Boolean;
    function Estrelas(Rank: String): Integer;
    function qntDuelos(NPC: Byte; hProcess: THandle; const Grava: Boolean = False; const QNT: Word = 0): Word;
    procedure TipoBonusReliquia(var Reliquia: fReliquia; IDs, IDFs: array of Word; card_Exist, card_ExistF: array of Byte; hProcess: THandle);
    function StatusCard(isCard: Byte): Byte;
    function StatusCardIsATKDEF(IsCard: Byte):Byte;
    function IsSubstituirCard(isCard: Byte; isCardF: array of Byte): Boolean;
    procedure notMegaMorph(hProcess: THandle);
  end;

type
  TDrop = record
  ID: Integer;
  NPC: byte;
  Duel: Integer;
  Prob: Real;
  TVit: Byte;
  end;

type
  TSymbol = array [1..10] of array [1..2] of Char;

type
  TPassword = array[0..23] of array[0..1] of Integer;

type
    TEmulador = record
      Nome : string;
      Code: array[0..9] of Byte;
      SunNumber: string;
    end;
// SOL 1 / LUA 2 / Dark 3 / AntiDark 4
// FOGO 5 / NATUREZA 6 / AR 7 / TERRA 8 / ELETRO 9 / AGUA 10
const
  FileCfg = 'Milenio.ini';
  AD_EPSXE = '444444';
  Emuladores: array[0..5] of TEmulador = (
  (Nome:'Epsxe 1.9.25';Code:(0,131,196,16,86,104,100,86,70,0);SunNumber:'P212224'),
  (Nome:'Epsxe 1.9.0';Code:(137,80,4,247,65,16,0,2,0,0);SunNumber:'P0'),
  (Nome:'Epsxe 1.8.0';Code:(71,40,0,0,0,0,232,225,27,0);SunNumber:'N19200'),
  (Nome:'Epsxe 1.7.0';Code:(0,0,0,128,125,38,139,207,211,235);SunNumber:'N1096064'),
  (Nome:'Epsxe 1.6.0';Code:(176,0,70,59,53,32,109,139,0,124);SunNumber:'N4852576'),
  (Nome:'Epsxe 1.5.2';Code:(165,255,36,149,88,69,68,0,139,199);SunNumber:'N4857184')
 // (Nome:'Emurayden 2.2';Code:(131,192,8,59,194,117,245,95,94,194);SunNumber:'')
  );
  SkinNome: array[0..2] of string = (
  ('MacMetal (internal)'),
  ('Wood (internal)'),
  ('WMP 2008 (internal)'));
  {$J+}
  Enigma: array[0..11] of fReliquia = (
  (ID:722;sAT:False;sATc:0;ATK:2800;DEF:2600;ATKG:800;DEFG:0;sTurn:0;sTurnE:0;sTurnB:False;TurnF:0;Tipo:1;Tp_Card:0;Tp_Monst:35),
  (ID:364;sAT:False;sATc:0;ATK:3000;DEF:2500;ATKG:600;DEFG:600;sTurn:0;sTurnE:0;sTurnB:False;TurnF:0;Tipo:4;Tp_Card:0;Tp_Monst:5),
  (ID:74;sAT:False;sATc:1;ATK:1300;DEF:2000;ATKG:0;DEFG:2500;sTurn:0;sTurnE:0;sTurnB:False;TurnF:3;Tipo:0;Tp_Card:0;Tp_Monst:0),
  (ID:74;sAT:False;sATc:2;ATK:1300;DEF:2000;ATKG:0;DEFG:2500;sTurn:0;sTurnE:0;sTurnB:False;TurnF:3;Tipo:0;Tp_Card:0;Tp_Monst:0),
  (ID:74;sAT:False;sATc:3;ATK:1300;DEF:2000;ATKG:0;DEFG:2500;sTurn:0;sTurnE:0;sTurnB:False;TurnF:3;Tipo:0;Tp_Card:0;Tp_Monst:0),
  (ID:74;sAT:False;sATc:4;ATK:1300;DEF:2000;ATKG:0;DEFG:2500;sTurn:0;sTurnE:0;sTurnB:False;TurnF:3;Tipo:0;Tp_Card:0;Tp_Monst:0),
  (ID:74;sAT:False;sATc:5;ATK:1300;DEF:2000;ATKG:0;DEFG:2500;sTurn:0;sTurnE:0;sTurnB:False;TurnF:3;Tipo:0;Tp_Card:0;Tp_Monst:0),
  (ID:7;sAT:False;sATc:1;ATK:1400;DEF:1200;ATKG:1600;DEFG:1800;sTurn:0;sTurnE:0;sTurnB:False;TurnF:1;Tipo:0;Tp_Card:0;Tp_Monst:0),
  (ID:7;sAT:False;sATc:2;ATK:1400;DEF:1200;ATKG:1600;DEFG:1800;sTurn:0;sTurnE:0;sTurnB:False;TurnF:1;Tipo:0;Tp_Card:0;Tp_Monst:0),
  (ID:7;sAT:False;sATc:3;ATK:1400;DEF:1200;ATKG:1600;DEFG:1800;sTurn:0;sTurnE:0;sTurnB:False;TurnF:1;Tipo:0;Tp_Card:0;Tp_Monst:0),
  (ID:7;sAT:False;sATc:4;ATK:1400;DEF:1200;ATKG:1600;DEFG:1800;sTurn:0;sTurnE:0;sTurnB:False;TurnF:1;Tipo:0;Tp_Card:0;Tp_Monst:0),
  (ID:7;sAT:False;sATc:5;ATK:1400;DEF:1200;ATKG:1600;DEFG:1800;sTurn:0;sTurnE:0;sTurnB:False;TurnF:1;Tipo:0;Tp_Card:0;Tp_Monst:0));
  Anel: array[0..10] of fReliquia = (
  (ID:114;sAT:False;sATc:1;ATK:1000;DEF:700;ATKG:500;DEFG:0;sTurn:0;sTurnE:0;sTurnB:False;TurnF:8;Tipo:11;Tp_Card:0;Tp_Monst:0),
  (ID:114;sAT:False;sATc:2;ATK:1000;DEF:700;ATKG:500;DEFG:0;sTurn:0;sTurnE:0;sTurnB:False;TurnF:8;Tipo:11;Tp_Card:0;Tp_Monst:0),
  (ID:114;sAT:False;sATc:3;ATK:1000;DEF:700;ATKG:500;DEFG:0;sTurn:0;sTurnE:0;sTurnB:False;TurnF:8;Tipo:11;Tp_Card:0;Tp_Monst:0),
  (ID:114;sAT:False;sATc:4;ATK:1000;DEF:700;ATKG:500;DEFG:0;sTurn:0;sTurnE:0;sTurnB:False;TurnF:8;Tipo:11;Tp_Card:0;Tp_Monst:0),
  (ID:114;sAT:False;sATc:5;ATK:1000;DEF:700;ATKG:500;DEFG:0;sTurn:0;sTurnE:0;sTurnB:False;TurnF:8;Tipo:11;Tp_Card:0;Tp_Monst:0),
  (ID:493;sAT:False;sATc:1;ATK:1550;DEF:1600;ATKG:1100;DEFG:1600;sTurn:0;sTurnE:0;sTurnB:False;TurnF:4;Tipo:0;Tp_Card:0;Tp_Monst:0),
  (ID:493;sAT:False;sATc:2;ATK:1550;DEF:1600;ATKG:1100;DEFG:1600;sTurn:0;sTurnE:0;sTurnB:False;TurnF:4;Tipo:0;Tp_Card:0;Tp_Monst:0),
  (ID:493;sAT:False;sATc:3;ATK:1950;DEF:1600;ATKG:1100;DEFG:1600;sTurn:0;sTurnE:0;sTurnB:False;TurnF:4;Tipo:0;Tp_Card:0;Tp_Monst:0),
  (ID:493;sAT:False;sATc:4;ATK:1950;DEF:1600;ATKG:1100;DEFG:1600;sTurn:0;sTurnE:0;sTurnB:False;TurnF:4;Tipo:0;Tp_Card:0;Tp_Monst:0),
  (ID:493;sAT:False;sATc:5;ATK:1950;DEF:1600;ATKG:1100;DEFG:1600;sTurn:0;sTurnE:0;sTurnB:False;TurnF:4;Tipo:0;Tp_Card:0;Tp_Monst:0),
  (ID:0;sAT:False;sATc:0;ATK:0;DEF:0;ATKG:600;DEFG:500;sTurn:0;sTurnE:0;sTurnB:False;TurnF:3;Tipo:3;Tp_Card:0;Tp_Monst:2));
  {$J-}
  DropList: array[0..81] of TDrop = (
              (ID:7;NPC:19;Duel:0;Prob:1;TVit:1),
              (ID:17;NPC:1;Duel:40;Prob:1;TVit:1),
              (ID:18;NPC:6;Duel:30;Prob:2;TVit:0),
              (ID:22;NPC:39;Duel:40;Prob:1;TVit:1),
              (ID:28;NPC:2;Duel:0;Prob:1;TVit:1),
              (ID:37;NPC:26;Duel:30;Prob:1;TVit:1),
              (ID:51;NPC:9;Duel:0;Prob:1;TVit:1),
              (ID:52;NPC:10;Duel:0;Prob:1;TVit:1),
              (ID:56;NPC:10;Duel:0;Prob:1;TVit:1),
              (ID:57;NPC:10;Duel:20;Prob:1;TVit:1),
              (ID:60;NPC:18;Duel:0;Prob:1;TVit:1),
              (ID:62;NPC:11;Duel:20;Prob:1;TVit:1),
              (ID:63;NPC:11;Duel:30;Prob:1;TVit:1),
              (ID:64;NPC:17;Duel:0;Prob:1;TVit:1),
              (ID:67;NPC:35;Duel:30;Prob:0.5;TVit:1),
              (ID:69;NPC:19;Duel:20;Prob:0.75;TVit:1),
              (ID:72;NPC:10;Duel:20;Prob:1;TVit:1),
              (ID:92;NPC:24;Duel:0;Prob:1;TVit:1),
              (ID:217;NPC:38;Duel:20;Prob:0.5;TVit:1),
              (ID:235;NPC:28;Duel:0;Prob:1;TVit:1),
              (ID:252;NPC:23;Duel:0;Prob:1;TVit:1),
              (ID:278;NPC:23;Duel:0;Prob:1;TVit:1),
              (ID:284;NPC:14;Duel:0;Prob:1;TVit:1),
              (ID:288;NPC:14;Duel:0;Prob:1;TVit:1),
              (ID:299;NPC:30;Duel:0;Prob:1;TVit:1),
              (ID:318;NPC:11;Duel:30;Prob:1.5;TVit:0),
              (ID:348;NPC:28;Duel:30;Prob:1;TVit:0),
              (ID:351;NPC:12;Duel:0;Prob:1;TVit:1),
              (ID:352;NPC:13;Duel:0;Prob:1;TVit:1),
              (ID:353;NPC:13;Duel:0;Prob:1;TVit:1),
              (ID:354;NPC:13;Duel:0;Prob:1;TVit:1),
              (ID:355;NPC:13;Duel:0;Prob:1;TVit:1),
              (ID:356;NPC:8;Duel:0;Prob:1;TVit:1),
              (ID:357;NPC:8;Duel:0;Prob:1;TVit:1),
              (ID:358;NPC:8;Duel:0;Prob:1;TVit:1),
              (ID:359;NPC:8;Duel:0;Prob:1;TVit:1),
              (ID:360;NPC:8;Duel:30;Prob:1;TVit:1),
              (ID:361;NPC:16;Duel:0;Prob:1;TVit:1),
              (ID:362;NPC:14;Duel:0;Prob:1;TVit:1),
              (ID:363;NPC:14;Duel:0;Prob:1;TVit:1),
              (ID:364;NPC:39;Duel:30;Prob:0.25;TVit:1),
              (ID:365;NPC:16;Duel:0;Prob:0.75;TVit:1),
              (ID:369;NPC:31;Duel:30;Prob:1;TVit:1),
              (ID:374;NPC:31;Duel:30;Prob:0.25;TVit:1),
              (ID:380;NPC:36;Duel:40;Prob:0.25;TVit:1),
              (ID:428;NPC:5;Duel:0;Prob:1;TVit:1),
              (ID:429;NPC:5;Duel:0;Prob:1;TVit:1),
              (ID:489;NPC:5;Duel:0;Prob:1;TVit:1),
              (ID:499;NPC:5;Duel:0;Prob:1;TVit:1),
              (ID:537;NPC:4;Duel:0;Prob:1;TVit:1),
              (ID:541;NPC:4;Duel:0;Prob:1;TVit:1),
              (ID:545;NPC:12;Duel:0;Prob:1;TVit:1),
              (ID:554;NPC:17;Duel:0;Prob:1;TVit:1),
              (ID:555;NPC:3;Duel:20;Prob:1;TVit:1),
              (ID:562;NPC:10;Duel:0;Prob:1;TVit:1),
              (ID:603;NPC:2;Duel:0;Prob:1;TVit:1),
              (ID:628;NPC:27;Duel:0;Prob:1;TVit:1),
              (ID:640;NPC:27;Duel:0;Prob:1;TVit:1),
              (ID:644;NPC:27;Duel:0;Prob:1;TVit:1),
              (ID:667;NPC:32;Duel:0;Prob:3;TVit:0),
              (ID:669;NPC:32;Duel:30;Prob:2;TVit:0),
              (ID:670;NPC:39;Duel:20;Prob:3;TVit:0),
              (ID:689;NPC:24;Duel:20;Prob:2;TVit:0),
              (ID:696;NPC:27;Duel:0;Prob:3;TVit:0),
              (ID:701;NPC:30;Duel:0;Prob:0.75;TVit:1),
              (ID:702;NPC:30;Duel:0;Prob:1;TVit:1),
              (ID:703;NPC:32;Duel:0;Prob:1;TVit:1),
              (ID:704;NPC:32;Duel:0;Prob:1;TVit:1),
              (ID:705;NPC:25;Duel:0;Prob:1;TVit:1),
              (ID:706;NPC:25;Duel:0;Prob:1;TVit:1),
              (ID:708;NPC:37;Duel:30;Prob:0.25;TVit:1),
              (ID:709;NPC:37;Duel:0;Prob:1;TVit:1),
              (ID:710;NPC:16;Duel:30;Prob:0.75;TVit:1),
              (ID:711;NPC:31;Duel:0;Prob:1;TVit:1),
              (ID:715;NPC:28;Duel:0;Prob:1;TVit:1),
              (ID:716;NPC:28;Duel:0;Prob:1;TVit:1),
              (ID:717;NPC:24;Duel:0;Prob:1;TVit:1),
              (ID:718;NPC:22;Duel:0;Prob:1;TVit:1),
              (ID:719;NPC:12;Duel:0;Prob:1;TVit:1),
              (ID:720;NPC:14;Duel:0;Prob:0.5;TVit:1),
              (ID:721;NPC:39;Duel:0;Prob:3;TVit:0),
              (ID:722;NPC:39;Duel:40;Prob:0.75;TVit:1));
  Passwords: TPassword = (
             (356,00000111),
             (360,11100000),
             (364,22200000),
             (365,33300000),
             (374,44400000),
             (380,55500000),
             (701,66600000),
             (702,77700000),
             (703,88800000),
             (704,99900000),
             (705,11110000),
             (706,22220000),
             (708,33330000),
             (709,44440000),
             (710,55550000),
             (713,66660000),
             (715,77770000),
             (716,88880000),
             (717,99990000),
             (718,11111000),
             (719,22222000),
             (720,33333000),
             (721,44444000),
             (722,55555000));
  SymbolADV: TSymbol = (
                ('2','3'), //1  SOL
                ('4','1'), //2  LUA
                ('1','4'), //3  DARK
                ('3','2'), //4  ANTIDARK
                ('6','A'), //5  FOGO
                ('7','5'), //6  NATUREZA
                ('8','6'), //7  AR
                ('9','7'), //8  TERRA
                ('A','8'), //9  ELETRO
                ('5','9'));//10 AGUA
  Biome: array[0..6] of string = ('Nenhum','Floresta','Deserto','Montanha','Campina','Oceano','Sombra');
  AD_CARD_YOU: array[1..10] of string = ('BFF510','BFF52C','BFF548','BFF564','BFF580','BFF59C','BFF5B8','BFF5D4','BFF5F0','BFF60C');
  AD_CARD_CPU: array[1..10] of string = ('BFF6B4','BFF6D0','BFF6EC','BFF708','BFF724','BFF740','BFF75C','BFF778','BFF794','BFF7B0');
  AD_PASS_NUM: array[1..8] of string = ('BC4DB0','BC4DB1','BC4DB2','BC4DB3','BC4DB4','BC4DB5','BC4DB6','BC4DB7');
  AD_CARD_OLHO = 'B419CF';
  AD_PASS_PRESS = '4F5210'; // 0 ou 1 - so funciona no 1.9.0
  AD_END_POS = 'B481F1'; // 3
  AD_END_MOVE = 'AF2D49'; // 20
  AD_PASS = 'BC4E7C';
  AD_PASS_MEGA = 'C2808A'; //64
  AD_PASS_REPEAT = 'C28038'; // size 90 bytes.
  AD_PASS_SHOW_CARD = 'BC4DC4'; // 1
  AD_PASS_IsSHOP= 'C580F5'; // 1
  AD_PASS_IsFORM: array[1..3] of string = ('B48120','B48200','B48040'); // 248, 248, 200
  AD_STAR_NUM = 'C28180';
  AD_STAR_Victory = 'BD13B2';
  AD_IsDuel = 'B4175E';
  AD_BIOMA = 'AF2D04';
  AD_TURN = 'B41991';
  AD_END = 'B4851C'; // 1
  AD_END_Receiv: array[1..3] of string = ('AF2CD8','C2D048','BD13B4'); // Name, Num, Shop
  AD_Adversary = 'AF2D01';
  AD_Form = 'AF2C0C'; //  duelo: 1731 password: 2506
  AD_VICTORYS = 'C280C0'; //2 BYTE WIN + LOST = HEX(dec((NUMBER NPC-1)*4))
  AD_SUBSTITUIR = 'BFF50E'; //54 = no campo, 55 = substituindo. 227 = atacando.

var
  Form1: TForm1;
  Config: fConfig;
  Codes: fCodes;
  CodesF: fCodes;
  eVersao: Byte;
  isGoPID: string;
  PID: THandle;
  selectBiome: Byte;
  bkListView: TcxListView;
  bStar: Boolean;
  bDrop: Boolean;
  bDropY: Boolean;
  bFinalView: Boolean;
  bFinalEnigma: Boolean;
  bGanhaSTEC: Boolean;
  iTurnAnel: Integer;
  bTurnAnel: Boolean;
  HKeyMinimize, HKeyDB, HKEYGuia: Integer;
  hFontYuGiOh : THandle;
  dataset: TStringList;

implementation

{$R ARQ_RECURSO.RES}
{$R ARQ_DBA.RES}
{$R ARQ_IMAGENS.RES}

uses Unit2, YGODB, Unit3, StrUtils;

{$R *.dfm}

procedure RotateBitmap(var hBitmapDC : Longint; var lWidth : Longint;
         var lHeight : Longint; lRadians : real);
var
   I, J, hNewBitmapDC, hNewBitmap: Longint;
   lSine, lCosine: extended;
   X1, X2, X3: Longint;
   Y1, Y2, Y3: Longint;
   lMinX, lMinY: Longint;
   lMaxX, lMaxY: Longint;
   lNewWidth, lNewHeight: Longint;
   lSourceX, lSourceY: Longint;
begin
 hNewBitmapDC := CreateCompatibleDC(hBitmapDC);
 lSine := Sin(lRadians);
 lCosine := Cos(lRadians);
 X1 := Round(-lHeight * lSine);
 Y1 := Round(lHeight * lCosine);
 X2 := Round(lWidth * lCosine - lHeight * lSine);
 Y2 := Round(lHeight * lCosine + lWidth * lSine);
 X3 := Round(lWidth * lCosine);
 Y3 := Round(lWidth * lSine);
 lMinX := Min(0, Min(X1, Min(X2, X3)));
 lMinY := Min(0, Min(Y1, Min(Y2, Y3)));
 lMaxX := Max(X1, Max(X2, X3));
 lMaxY := Max(Y1, Max(Y2, Y3));
 lNewWidth := lMaxX - lMinX;
 lNewHeight := lMaxY - lMinY;
 hNewBitmap := CreateCompatibleBitmap(hBitmapDC, lNewWidth, lNewHeight);
 SelectObject(hNewBitmapDC, hNewBitmap);
 For I := 0 To lNewHeight do begin
    For J := 0 To lNewWidth do begin
       lSourceX := Round((J + lMinX) * lCosine + (I + lMinY) * lSine);
       lSourceY := Round((I + lMinY) * lCosine - (J + lMinX) * lSine);
       If (lSourceX >= 0) And (lSourceX <= lWidth) And
       (lSourceY >= 0) And (lSourceY <= lHeight) Then
           BitBlt(hNewBitmapDC, J, I, 1, 1, hBitmapDC,
                      lSourceX, lSourceY, SRCCOPY);
    end;
 end;
 lWidth := lNewWidth;
 lHeight := lNewHeight;
 hBitmapDC := hNewBitmapDC;
 DeleteObject(hNewBitmap);
end;

class procedure TForm1.reproduzirAudio(const tipo: Integer = 0);
const
  SOM: array[0..3] of String = ('AUDIOINJ', 'AUDIOOFF',
                                'DRENALP', 'MUDASTEC');
var
   hFind, hRes: THandle;
   Song: PChar;
begin
  hFind := FindResource(hInstance, PChar(SOM[tipo]), 'WAVE');
  if hFind <> 0 then begin
    hRes:=LoadResource(hInstance, hFind);
    if hRes <> 0 then begin
      Song:=LockResource(hRes);
      if Assigned(Song) then SndPlaySound(Song, snd_ASync or snd_Memory) ;
      UnlockResource(hRes);
    end;
    FreeResource(hFind);
  end;
end;

procedure TForm1.visualizarImagem(ID: Integer; var IMG: TImage; const PeB: Boolean = False; const qualidade: Integer = 100);
var
   Stream: TResourceStream;
   desc: string;
begin
  desc := 'CARTA' + FormatFloat('000', ID);
  try
    if (ID >= 1) and (ID <= 722) then
    begin
      Stream := TResourceStream.Create(hInstance, PChar(desc), RT_RCDATA);
      IMG.Picture.Graphic := TJPEGImage.Create;

      with (IMG.Picture.Graphic AS TJPEGImage) do
      begin
        Grayscale := PeB;
        CompressionQuality := qualidade;
        Performance := jpBestSpeed;
      end;

      IMG.Picture.Graphic.LoadFromStream(Stream);
      Stream.Free;
    end else IMG.Picture := nil;
  except
    IMG.Picture := nil;
  end;
end;

{Function ExtractWindowsDir : String;
Var Buffer : Array[0..144] of Char;
Begin
 GetWindowsDirectory(Buffer,144);
 Result := IncludeTrailingBackSlash(StrPas(Buffer));
End; }

procedure TForm1.ScaleForm(F: TForm; Tamanho: Integer);
const
  nTamOriginalWd = 559;
  nTamOriginalHt = 191;
var
  nEscala, WD, HG : Integer;
begin
  if (Config.escala) then
  begin
    F.Scaled := true;
    WD := Screen.Width - trunc(Screen.Width * -Tamanho div 100);
    HG := Screen.Width - trunc(nTamOriginalWd * -Tamanho div 100);
    F.ScaleBy(WD + Config.escalaPos, HG + Config.escalaPos);
    sPanelRun.Left := sPanelBG.Left + sPanelC.Width;
    sStatusBar1.Panels[2].Text := Format('X: %d, Y: %d', [F.Width, F.Height]);
  end;
end;

function TForm1.UnderWine: Boolean;
var H: cardinal;
begin
  Result := False;
  H := LoadLibrary('ntdll.dll');
  if H > HINSTANCE_ERROR then
    begin
      Result := Assigned(GetProcAddress(H, 'wine_get_version'));
      FreeLibrary(H);
    end;
end;

function InserirFonteMemoria: Boolean;
var
   Stream: TResourceStream;
   FontsCount : integer;
begin
  // Stream1 := TResourceStream.Create(hInstance,'FONTMS', RT_RCDATA);
  // Stream2 := TResourceStream.Create(hInstance,'FONTWEB', RT_RCDATA);
   Stream := TResourceStream.Create(hInstance,'FONTYG', RT_RCDATA);
   hFontYuGiOh := AddFontMemResourceEx(Stream.Memory, Stream.Size, nil, @FontsCount);
   result := (hFontYuGiOh <> 0);
   Stream.Free;
end;


procedure TForm1.OpacidadeIMG(Bmp: TBitmap; Count: Byte);
var
  x, y: Integer;
  cor: LongInt;
  R, G, B: Byte;
begin
  for x:= 0 to Bmp.Width-1 do
    for y:= 0 to Bmp.Height-1 do
     begin
        cor := ColorToRGB(Bmp.Canvas.Pixels[x,y]);
         R := Count + Trunc(GetRValue(cor) * 0.5);
         G := Count + Trunc(GetGValue(cor) * 0.5);
         B := Count + Trunc(GetBValue(cor) * 0.5);
        Bmp.Canvas.Pixels[x,y] := RGB(R,G,B);
     end;
end;

procedure TForm1.img1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
const
 sc_DragMove = $f012;
begin
 ReleaseCapture;
 Perform(wm_SysCommand, sc_DragMove, 0);
end;

procedure TForm1.WMHotKey(var Msg: TWMHotKey);
begin
    if Msg.HotKey = HKeyMinimize then
    if WindowState = wsNormal then
         Form1.WindowState := wsMinimized else
       begin
         Form1.WindowState := wsNormal;
       end;

    if Msg.HotKey = HKEYGuia then
    if Form3.Showing then
       Form3.Close else
       Form3.Show;

    if Msg.HotKey = HKeyDB then
    if FormDB.Showing then
       FormDB.Close else
       FormDB.Show;
end;

procedure TForm1.lbCloseClick(Sender: TObject);
begin
 Close;
end;

function UsuarioLogado: String;
var
  I: DWord;
  user: string;
begin
  I := 255;
  SetLength(user, I);
  Windows.GetUserName(PChar(user), I);
  user := string(PChar(user));
  result := user;
end;

procedure TForm1.lerConfig;
var
  ArquivoINI: TIniFile;
  fileINI: string;
begin
  fileINI := ExtractFileDir(ParamStr(0))+'\'+FileCfg;
  ArquivoINI := TIniFile.Create(fileINI);
  try
    if ArquivoINI.SectionExists('CONFIGURACAO') then
    begin
     Config.IsViewMonster := ArquivoINI.ReadInteger('CONFIGURACAO','IsViewMonster',1);
     Config.IsViewOther := ArquivoINI.ReadInteger('CONFIGURACAO','IsViewOther',0);
     Config.IsViewAlpha := ArquivoINI.ReadInteger('CONFIGURACAO','IsViewAlpha',0);
     Config.IsInterval := ArquivoINI.ReadInteger('CONFIGURACAO','IsInterval',100);
     Config.IsStarReceiv := ArquivoINI.ReadBool('CONFIGURACAO','IsStarReceiv',True);
     Config.IsNotLimitedCard := ArquivoINI.ReadBool('CONFIGURACAO','IsNotLimitedCard',True);
     Config.IsYesPassword := ArquivoINI.ReadBool('CONFIGURACAO','IsYesPassword',True);
     Config.IsRankPreview := ArquivoINI.ReadBool('CONFIGURACAO','IsRankPreview',True);
     Config.IsReliquiasOn := ArquivoINI.ReadBool('CONFIGURACAO','IsReliquiasOn',True);     
     Config.IsReliquiasGet := ArquivoINI.ReadInteger('CONFIGURACAO','IsReliquiasGet',3);
     Config.IsReliquiaGifMove := ArquivoINI.ReadBool('CONFIGURACAO','IsReliquiaGifMove',True);
     Config.isDropMOD := ArquivoINI.ReadBool('CONFIGURACAO','isDropMOD',True);
     Config.IsModDEF := ArquivoINI.ReadBool('CONFIGURACAO','IsModDEF',False);
     Config.IsOpaque := ArquivoINI.ReadBool('CONFIGURACAO','IsOpaque',False);
     Config.IsPeB := ArquivoINI.ReadBool('CONFIGURACAO','IsPeB',False);
     Config.IsDebug77 := ArquivoINI.ReadBool('CONFIGURACAO','IsDebug77',False);
     Config.wSkin := ArquivoINI.ReadInteger('CONFIGURACAO','wSkin',0);
     Config.wEmu := ArquivoINI.ReadInteger('CONFIGURACAO','wEmu',-1);
     Config.wMax := ArquivoINI.ReadBool('CONFIGURACAO','wMax',False);
     Config.wHotKeys := ArquivoINI.ReadBool('CONFIGURACAO','wHotKeys',True);
     Config.wDBMax := ArquivoINI.ReadBool('CONFIGURACAO','wDBMax',False);
     Config.wDBImgShow := ArquivoINI.ReadBool('CONFIGURACAO','wDBImgShow',DirectoryExists(ExtractFileDir(ParamStr(0))+'\imgs'));
     Config.wPosX := ArquivoINI.ReadInteger('CONFIGURACAO','wPosX',0);
     Config.wPosY := ArquivoINI.ReadInteger('CONFIGURACAO','wPosY',0);
     Config.escala := ArquivoINI.ReadBool('CONFIGURACAO','escala',False);
     Config.escalaPos := ArquivoINI.ReadInteger('CONFIGURACAO','escalaPos',0);
    end else
    begin
     Config.IsViewMonster := 1;
     Config.IsViewOther := 0;
     Config.IsViewAlpha := 0;
     Config.IsInterval := 100;
     Config.IsStarReceiv := True;
     Config.IsNotLimitedCard := True;
     Config.IsYesPassword := True;
     Config.IsRankPreview := True;
     Config.IsReliquiasOn := True;
     Config.IsReliquiasGet := 3;
     Config.isDropMOD := True;
     Config.wSkin := 0;
     Config.wEmu := -1;
     Config.wPosX := 0;
     Config.wPosY := 0;
     Application.MessageBox(PChar(
     'Seja Bem-Vindo '+UsuarioLogado+#13+'Antes de utilizar o m�dulo, por gentileza, verifique as configura��es do aplicativo e tamb�m veja o Guia de Instru��es! Bom Jogo.')
     ,'Ol�!',0);
     btCfg.Click;
     btnInfo.Click;
     Form2.Left := 0;
     Form2.Top := Form1.Height + 20;
     Form3.Left := Form2.Width + 20;
     Form3.Top := 0;
    end;
  finally
    FreeAndNil(ArquivoINI);
  end;
end;

procedure TForm1.salvarConfig(const auto: Boolean = False);
const
  MSGError: array[1..3] of String = ('Ocorreu erro na leitura de configura��es.',
                                     'Ocorreu erro na leitura de c�digos.',
                                     'Ocorreu erro na leitura do arquivo.');
var
  ArquivoINI: TIniFile;
  fileINI: string;
begin
  fileINI := ExtractFileDir(ParamStr(0))+'\'+FileCfg;
  ArquivoINI := TIniFile.Create(fileINI);
  try
   if not auto then
   begin
    ArquivoINI.WriteInteger('CONFIGURACAO','IsViewMonster',Config.IsViewMonster);
    ArquivoINI.WriteInteger('CONFIGURACAO','IsViewOther',Config.IsViewOther);
    ArquivoINI.WriteInteger('CONFIGURACAO','IsViewAlpha',Config.IsViewAlpha);
    ArquivoINI.WriteInteger('CONFIGURACAO','IsInterval',Config.IsInterval);
    ArquivoINI.WriteBool('CONFIGURACAO','IsStarReceiv',Config.IsStarReceiv);
    ArquivoINI.WriteBool('CONFIGURACAO','IsNotLimitedCard',Config.IsNotLimitedCard);
    ArquivoINI.WriteBool('CONFIGURACAO','IsYesPassword',Config.IsYesPassword);
    ArquivoINI.WriteBool('CONFIGURACAO','IsRankPreview',Config.IsRankPreview);
    ArquivoINI.WriteBool('CONFIGURACAO','IsReliquiasOn',Config.IsReliquiasOn);
    ArquivoINI.WriteInteger('CONFIGURACAO','IsReliquiasGet',Config.IsReliquiasGet);
    ArquivoINI.WriteBool('CONFIGURACAO','IsReliquiaGifMove',Config.IsReliquiaGifMove);
    ArquivoINI.WriteBool('CONFIGURACAO','isDropMOD',Config.isDropMOD);
    ArquivoINI.WriteBool('CONFIGURACAO','IsModDEF',Config.IsModDEF);
    ArquivoINI.WriteBool('CONFIGURACAO','IsOpaque',Config.IsOpaque);
    ArquivoINI.WriteBool('CONFIGURACAO','IsPeB',Config.IsPeB);
    ArquivoINI.WriteBool('CONFIGURACAO','escala',Config.escala);
    ArquivoINI.WriteInteger('CONFIGURACAO','escalaPos',Config.escalaPos);
   end else
   begin
    ArquivoINI.WriteBool('CONFIGURACAO','wMax',Config.wHotKeys);
    ArquivoINI.WriteInteger('CONFIGURACAO','wSkin',Config.wSkin);
    ArquivoINI.WriteInteger('CONFIGURACAO','wEmu',Config.wEmu);
    ArquivoINI.WriteBool('CONFIGURACAO','wMax',Config.wMax);
    ArquivoINI.WriteBool('CONFIGURACAO','wDBMax',Config.wDBMax);
    ArquivoINI.WriteBool('CONFIGURACAO','wDBImgShow',Config.wDBImgShow);
    ArquivoINI.WriteInteger('CONFIGURACAO','wPosX',Config.wPosX);
    ArquivoINI.WriteInteger('CONFIGURACAO','wPosY',Config.wPosY);
   end;
  finally
   FreeAndNil(ArquivoINI);
  end;
end;

procedure TForm1.btCfgClick(Sender: TObject);
begin
   if Form2.Showing then
     Form2.Close else
     Form2.Show;
end;

procedure TForm1.btATClick(Sender: TObject);
var
  i: Integer;
  imgTemp: TImage;
begin
 if TIProcess.Enabled then
 begin
   TIProcess.Enabled := False;
   btAT.Caption := 'Ativar';
   reproduzirAudio(1);
   if (Config.IsReliquiasOn) then
      img1.Picture := cxImgColecaoDesativado.Items[Config.IsReliquiasGet].Picture
   else img1.Picture := cxImgColecaoDesativado.Items[4].Picture;
   for i:= 1 to 10 do
   begin
     imgTemp := (Form1.FindComponent(FormatFloat('img00', i)) as TImage);
     imgTemp.Picture := nil;
     imgTemp.Tag := 0;
     imgTemp.HelpContext := 0;
   end;
 end else
 begin
  sPanelRun.Visible := not sPanelRun.Visible;
  sPanelRun.BringToFront;
  Application.ProcessMessages;
  ListarProcessosAbertos;
  cbEmulador.ReadOnly := not sCheckEmu.Checked;
  cbEmulador.Clear;

  for i:= 0 to Length(Emuladores)-1 do
  cbEmulador.Items.Add(Emuladores[i].Nome);

  if cbEmulador.Items.Count < Config.wEmu then
     cbEmulador.ItemIndex := Config.wEmu else
     cbEmulador.ItemIndex := -1;
     
  if cbEmulador.ItemIndex < 0 then
  begin
    cbEmulador.ItemIndex := -1;
    cbEmulador.Text := 'Selecionar Emulador Manualmente...'; 
  end;

  for i:=0 to cbProcessos.Items.Count-1 do
  if AnsiPos(ChangeFileExt(isGoPID,''),cbProcessos.Items.Strings[i]) > 0 then
  begin
    cbProcessos.ItemIndex := i;
    Break;
  end;
  verificarEmulador;
 end;
end;

procedure TForm1.FormShow(Sender: TObject);
 procedure porcentar(var FormTemp: TForm; var loading: TsGauge; pos: Integer);
 begin
   SetForegroundWindow(FormTemp.Handle);
   loading.Progress := pos;
 end;
var
  escalaIni: Integer;
  posForm : Integer;
  loading: TsGauge;
  FormTemp: TForm;
begin
 FormTemp := TForm.Create(Screen);
 loading := TsGauge.Create(Screen);
 try
   FormTemp.Color := clBlack;
   FormTemp.TransparentColor := True;
   FormTemp.TransparentColorValue := clBlack;
   FormTemp.Parent := Screen.ActiveForm;
   FormTemp.DoubleBuffered := True;
   FormTemp.BorderStyle := bsNone;
   loading.Height := 30;
   loading.Width := 360;
   FormTemp.Left := Screen.WorkAreaWidth div 2 - 180;
   FormTemp.Top := Screen.WorkAreaHeight div 2 - 15;
   loading.Suffix := '% - Carregando...';
   loading.Parent := FormTemp;
   FormTemp.AutoSize := True;
   FormTemp.Show;
   porcentar(FormTemp, loading, 0);
   lerConfig;

   if (Config.wPosX >= Screen.WorkAreaLeft) and
      ((Config.wPosX+Form1.Width) <= Screen.WorkAreaWidth) and
      (Config.wPosY >= Screen.WorkAreaTop) and
      ((Config.wPosY+Form1.Height) <= Screen.WorkAreaHeight) then
   begin
    Form1.Left := Config.wPosX;
    Form1.Top := Config.wPosY;
   end else
   begin
    Form1.Left := (Screen.Monitors[0].Width div 2) - (Form1.Width div 2);
    Form1.Top := (Screen.Monitors[0].Height div 2) - (Form1.Height div 2);
   end;
   porcentar(FormTemp, loading, 15);
   posForm := Form1.Left;
   Form1.Left := -10000;
   executarConfig;

   if not Assigned(bkListView) then
   begin
    bkListView := TcxListView.Create(nil);
    bkListView.Visible := False;
    bkListView.Left := 0;
    bkListView.Top := 0;
    bkListView.ViewStyle := vsReport;
    bkListView.Parent := Form1;
    bkListView.Columns := FormDB.ListView1.Columns;
    porcentar(FormTemp, loading, 21);
    Config.wMax := not Config.wMax; 
    lbReduze.OnClick(Self);
    InserirFonteMemoria;
    porcentar(FormTemp, loading, 29);
    if (Config.escalaPos < -5) then
        Config.escalaPos := -5;
    if (Config.escalaPos > 20) then
        Config.escalaPos := 20;
      if (Config.escala) then
      begin
        escalaIni := Config.escalaPos;
        while escalaIni <> 0 do
        begin
          if (escalaIni <= 0) then
          begin
            escalaIni:= escalaIni+ 1;
            if (escalaIni <> 0) then
                ScaleForm(Form1, -5);
          end else
          begin
            escalaIni := escalaIni - 1;
            if (escalaIni <> 0) then
                ScaleForm(Form1, 5);
          end;
        end;
      end;
   end;

   porcentar(FormTemp, loading, 35);
   if Config.IsDebug77 then
   begin
    lbDescBiome.Cursor := crHandPoint;
    Form1.Left := Form1.Left + sPaneDeb.Width;
    porcentar(FormTemp, loading, 43);
    lbDescBiome.OnClick(lbDescBiome);
    lbDescBiome.Hint := '<= Abrir Janela de Debug.';
   end;

   Form1.AutoSize := False;
   porcentar(FormTemp, loading, 55);
   Form1.Height := pnlDebug.Height + 5;
   bkListView.Visible := False;
   bkListView.Clear;
   FormDB.Procurar(0,'','');
   porcentar(FormTemp, loading, 67);
   bkListView.Items := FormDB.ListView1.Items;
   porcentar(FormTemp, loading, 80);
   lbReduze.OnClick(lbReduze);
   Sleep(200);
   lbReduze.OnClick(lbReduze);
   porcentar(FormTemp, loading, 99);
 finally
   FreeAndNil(loading);
   FreeAndNil(FormTemp);
   Form1.Left := posForm;
 end;
end;

procedure TForm1.executarConfig;
begin
 if Config.wHotKeys then
 begin
   HKeyMinimize := GlobalAddAtom('HKeyMinimize');
   RegisterHotKey(Handle, HKeyMinimize, MOD_WIN, VK_NUMPAD1);
   HKeyDB:= GlobalAddAtom('HKeyDB');
   RegisterHotKey(Handle, HKeyDB, MOD_WIN, VK_NUMPAD0);
   HKEYGuia:= GlobalAddAtom('HKEYGuia');
   RegisterHotKey(Handle, HKEYGuia, MOD_WIN, VK_NUMPAD2);
 end else
 begin
   UnRegisterHotKey(Handle, HKeyMinimize);
   GlobalDeleteAtom(HKeyMinimize);
   UnRegisterHotKey(Handle, HKeyDB);
   GlobalDeleteAtom(HKeyDB);
   UnRegisterHotKey(Handle, HKEYGuia);
   GlobalDeleteAtom(HKEYGuia);
 end;

 if TIProcess.Enabled then
    btAT.Click;

 case Config.IsViewAlpha of
   0: Form1.AlphaBlendValue := 255;
   1: Form1.AlphaBlendValue := 235;
   2: Form1.AlphaBlendValue := 215;
   3: Form1.AlphaBlendValue := 195;
   4: Form1.AlphaBlendValue := 175;
 end;

 img1.Picture := cxImgColecaoDesativado.Items[Config.IsReliquiasGet].Picture;

 if not Config.IsReliquiasOn or (Config.IsReliquiasGet > 3) then
    img1.Picture := cxImgColecaoDesativado.Items[4].Picture;

 if not Config.IsReliquiasOn then
 sIconList.GetIcon(4, Application.Icon) else
 sIconList.GetIcon(Config.IsReliquiasGet, Application.Icon);

 if Config.IsReliquiasOn then
 begin
   sPanelM.Visible := True;
   sPanelO.Visible := True;
 end else
 begin
   sPanelM.Visible := False;
   sPanelO.Visible := False;
 end;

 case Config.IsReliquiasGet of
   0: begin
        Codes.Is1 := AD_CARD_CPU[1];
        Codes.Is2 := AD_CARD_CPU[2];
        Codes.Is3 := AD_CARD_CPU[3];
        Codes.Is4 := AD_CARD_CPU[4];
        Codes.Is5 := AD_CARD_CPU[5];
        Codes.Is6 := AD_CARD_CPU[6];
        Codes.Is7 := AD_CARD_CPU[7];
        Codes.Is8 := AD_CARD_CPU[8];
        Codes.Is9 := AD_CARD_CPU[9];
        Codes.Is10 := AD_CARD_CPU[10];
        CodesF.Is1 := AD_CARD_YOU[1];
        CodesF.Is2 := AD_CARD_YOU[2];
        CodesF.Is3 := AD_CARD_YOU[3];
        CodesF.Is4 := AD_CARD_YOU[4];
        CodesF.Is5 := AD_CARD_YOU[5];
        CodesF.Is6 := AD_CARD_YOU[6];
        CodesF.Is7 := AD_CARD_YOU[7];
        CodesF.Is8 := AD_CARD_YOU[8];
        CodesF.Is9 := AD_CARD_YOU[9];
        CodesF.Is10 := AD_CARD_YOU[10];
        lbSymbol1.Font.Name := 'YuGiOh';
        lbSymbol2.Font.Name := 'YuGiOh';
        lbSymbol3.Font.Name := 'YuGiOh';
        lbSymbol4.Font.Name := 'YuGiOh';
        lbSymbol5.Font.Name := 'YuGiOh';
      end;
 else
      begin
        Codes.Is1 := AD_CARD_YOU[1];
        Codes.Is2 := AD_CARD_YOU[2];
        Codes.Is3 := AD_CARD_YOU[3];
        Codes.Is4 := AD_CARD_YOU[4];
        Codes.Is5 := AD_CARD_YOU[5];
        Codes.Is6 := AD_CARD_YOU[6];
        Codes.Is7 := AD_CARD_YOU[7];
        Codes.Is8 := AD_CARD_YOU[8];
        Codes.Is9 := AD_CARD_YOU[9];
        Codes.Is10 := AD_CARD_YOU[10];
        CodesF.Is1 := AD_CARD_CPU[1];
        CodesF.Is2 := AD_CARD_CPU[2];
        CodesF.Is3 := AD_CARD_CPU[3];
        CodesF.Is4 := AD_CARD_CPU[4];
        CodesF.Is5 := AD_CARD_CPU[5];
        CodesF.Is6 := AD_CARD_CPU[6];
        CodesF.Is7 := AD_CARD_CPU[7];
        CodesF.Is8 := AD_CARD_CPU[8];
        CodesF.Is9 := AD_CARD_CPU[9];
        CodesF.Is10 := AD_CARD_CPU[10];
        lbSymbol1.Font.Name := 'Tahoma';
        lbSymbol2.Font.Name := 'Tahoma';
        lbSymbol3.Font.Name := 'Tahoma';
        lbSymbol4.Font.Name := 'Tahoma';
        lbSymbol5.Font.Name := 'Tahoma';
      end;
 end;

 TIProcess.Interval := Config.IsInterval;
 if Config.wSkin = 255 then
  sSkinManager1.Active := False else
 begin
   if sSkinManager1.SkinName  <> SkinNome[Config.wSkin] then
    sSkinManager1.SkinName := SkinNome[Config.wSkin];
   sSkinManager1.Active := True;
   lbDescBiome.Font.Color := clBlack;
   if (Config.wSkin = 2) then begin
     lbDescBiome.Font.Color := clWhite;
   end;
 end;

 case Config.IsViewMonster of
   0: begin
      img01.Visible := True;
      img02.Visible := True;
      img03.Visible := True;
      img04.Visible := True;
      img05.Visible := True;
      lbID1.Visible := False;
      lbID2.Visible := False;
      lbID3.Visible := False;
      lbID4.Visible := False;
      lbID5.Visible := False;
      lbATK1.Visible := False;
      lbATK2.Visible := False;
      lbATK3.Visible := False;
      lbATK4.Visible := False;
      lbATK5.Visible := False;
      lbDEF1.Visible := False;
      lbDEF2.Visible := False;
      lbDEF3.Visible := False;
      lbDEF4.Visible := False;
      lbDEF5.Visible := False;
      lbSymbol1.Visible := False;
      lbSymbol2.Visible := False;
      lbSymbol3.Visible := False;
      lbSymbol4.Visible := False;
      lbSymbol5.Visible := False;
      end;
   1: begin
      img01.Visible := False;
      img02.Visible := False;
      img03.Visible := False;
      img04.Visible := False;
      img05.Visible := False;
      lbID1.Visible := True;
      lbID2.Visible := True;
      lbID3.Visible := True;
      lbID4.Visible := True;
      lbID5.Visible := True;
      lbATK1.Visible := True;
      lbATK2.Visible := True;
      lbATK3.Visible := True;
      lbATK4.Visible := True;
      lbATK5.Visible := True;
      lbDEF1.Visible := True;
      lbDEF2.Visible := True;
      lbDEF3.Visible := True;
      lbDEF4.Visible := True;
      lbDEF5.Visible := True;
      lbSymbol1.Visible := True;
      lbSymbol2.Visible := True;
      lbSymbol3.Visible := True;
      lbSymbol4.Visible := True;
      lbSymbol5.Visible := True;
      end;
   2: begin
      img01.Visible := True;
      img02.Visible := True;
      img03.Visible := True;
      img04.Visible := True;
      img05.Visible := True;
      lbID1.Visible := True;
      lbID2.Visible := True;
      lbID3.Visible := True;
      lbID4.Visible := True;
      lbID5.Visible := True;
      lbATK1.Visible := True;
      lbATK2.Visible := True;
      lbATK3.Visible := True;
      lbATK4.Visible := True;
      lbATK5.Visible := True;
      lbDEF1.Visible := True;
      lbDEF2.Visible := True;
      lbDEF3.Visible := True;
      lbDEF4.Visible := True;
      lbDEF5.Visible := True;
      lbSymbol1.Visible := True;
      lbSymbol2.Visible := True;
      lbSymbol3.Visible := True;
      lbSymbol4.Visible := True;
      lbSymbol5.Visible := True;      
      end;
 end;

 case Config.IsViewOther of
   0: begin
      img06.Visible := True;
      img07.Visible := True;
      img08.Visible := True;
      img09.Visible := True;
      img10.Visible := True;
      lbID6.Visible := False;
      lbID7.Visible := False;
      lbID8.Visible := False;
      lbID9.Visible := False;
      lbID10.Visible := False;
      lbTP6.Visible := False;
      lbTP7.Visible := False;
      lbTP8.Visible := False;
      lbTP9.Visible := False;
      lbTP10.Visible := False;
      end;
   1: begin
      img06.Visible := False;
      img07.Visible := False;
      img08.Visible := False;
      img09.Visible := False;
      img10.Visible := False;
      lbID6.Visible := True;
      lbID7.Visible := True;
      lbID8.Visible := True;
      lbID9.Visible := True;
      lbID10.Visible := True;
      lbTP6.Visible := True;
      lbTP7.Visible := True;
      lbTP8.Visible := True;
      lbTP9.Visible := True;
      lbTP10.Visible := True;
      end;
   2: begin
      img06.Visible := True;
      img07.Visible := True;
      img08.Visible := True;
      img09.Visible := True;
      img10.Visible := True;
      lbID6.Visible := True;
      lbID7.Visible := True;
      lbID8.Visible := True;
      lbID9.Visible := True;
      lbID10.Visible := True;
      lbTP6.Visible := True;
      lbTP7.Visible := True;
      lbTP8.Visible := True;
      lbTP9.Visible := True;
      lbTP10.Visible := True;
      end;
 end;
end;

function PIDExists(PID: Integer): Boolean;
var 
  ProcEntry: TProcessEntry32;
  Hnd: THandle;
  Fnd: Boolean;
  cHandle: Cardinal;
begin
  Result := False;
  Hnd := CreateToolhelp32Snapshot(TH32CS_SNAPALL, 0);
  cHandle := Cardinal(Hnd);
  if (cHandle <> -1) then
  begin
    ProcEntry.dwSize := SizeOf(TProcessEntry32); 
    Fnd := Process32First(cHandle, ProcEntry);
    while Fnd do
    begin
      Fnd := Process32Next(cHandle, ProcEntry);
      if (ProcEntry.th32ProcessID = PID) then
      begin
        Result := True;
        Break;
      end;
    end;
    CloseHandle(Hnd);
  end;
end;

procedure TForm1.ListarProcessosAbertos;
var 
  ProcEntry: TProcessEntry32; 
  Hnd: THandle; 
  Fnd: Boolean;
  i: Byte;
  cHandle: Cardinal;
begin
  Hnd := CreateToolhelp32Snapshot(TH32CS_SNAPALL, 0);
  cHandle := Cardinal(Hnd);
  cbProcessos.Clear;
  if (cHandle <> -1) then
  begin
    ProcEntry.dwSize := SizeOf(TProcessEntry32);
    Fnd := Process32First(Hnd, ProcEntry);
    i := 0;

    while Fnd do
    begin
      i := i + 1;
      if i > 50 then
      i := 0;
      Form1.sLabelRunEmu.Caption := 'Carregando os Processos   ' + StringOfChar('.',i);
      if (i mod 2) = 0 then
      Application.ProcessMessages;
      Fnd := Process32Next(cHandle, ProcEntry);
      if (AnsiPos('.exe',ProcEntry.szExeFile) > 0) or UnderWine then
      cbProcessos.Items.Add(ProcEntry.szExeFile+' ['+IntToStr(ProcEntry.th32ProcessID)+']');
    end;
    CloseHandle(Hnd);
  end;
  cbProcessos.Sorted := True;
  cbProcessos.ItemIndex := 0;
end;

procedure TForm1.sWebLabel1Click(Sender: TObject);
begin
 sPanelRun.Visible := False;
end;

procedure TForm1.sWebLabel2Click(Sender: TObject);
begin
 ListarProcessosAbertos;
 verificarEmulador;
end;

procedure TForm1.TIProcessTimer(Sender: TObject);
var
  hProcess: THandle;
  //Struct: TProcessEntry32;
  PassNumber: array[1..8] of Byte;
  temp: cardinal;
  starCount, receivStar, i, l: Integer;
  IsGeneric, isSubstituir, isShop, IsViraCard, isNPC, isEND, isENDVit, isPASS_Form1, isPASS_Form2, isPASS_Form3: Byte;
  isCard, isCardF: array[1..10] of Byte;
  bufID, bufIDF: array[1..10] of Word;
  bufBiome,bufLPYOU,bufLPCPU,bufATK,bufDEF,bufEQP,bufBIO,bufIsDuel,bufIsTurno,IsEnd_Card, gCard: Word;
  s: string;
begin
 if PIDExists(PID) then
 begin
  hProcess := OpenProcess(PROCESS_ALL_ACCESS,FALSE,PID);
  //getmem(buf,4);
  //ReadProcessMemory(hProcess,ptr($+(int Codes.Is1)), Addr(buf),2,temp);
  ReadProcessMemory(hProcess,ptr(GetEmule(AD_STAR_NUM)), Addr(starCount),4,temp);
  sStatusBar1.Panels.Items[1].Text := FormatFloat('Estrelas: 000000',starCount);
  ReadProcessMemory(hProcess,ptr(GetEmule(AD_IsDuel)), Addr(bufIsDuel),1,temp);

  if Config.IsDebug77 then
  begin
   if sCheckDeckYou.Checked then
   begin
    IsGeneric := 40 - StrToInt(sCheckDeckYou.Caption);
    WriteProcessMemory(hProcess,ptr(GetEmule($B419A8)), Addr(IsGeneric),1,temp);
   end;

   // deck You;
   ReadProcessMemory(hProcess,ptr(GetEmule($B419A8)), Addr(IsGeneric),1,temp);
   sCheckDeckYou.Caption := IntToStr(40 - IsGeneric);

   if sCheckDeckCPU.Checked then
   begin
    IsGeneric := 40 - StrToInt(sCheckDeckCPU.Caption);
    WriteProcessMemory(hProcess,ptr(GetEmule($B419C8)), Addr(IsGeneric),1,temp);
   end;

   ReadProcessMemory(hProcess,ptr(GetEmule($B419C8)), Addr(IsGeneric),1,temp);
   sCheckDeckCPU.Caption := IntToStr(40 - IsGeneric);

   if sCheckLPCPU.Checked then
   begin
    bufLPCPU := StrToInt(sCheckLPCPU.Caption);
    WriteProcessMemory(hProcess,ptr(GetEmule($B419C4)), Addr(bufLPCPU),2,temp);
   end;

   ReadProcessMemory(hProcess,ptr(GetEmule($B419C4)), Addr(bufLPCPU),2,temp);
   sCheckLPCPU.Caption := IntToStr(bufLPCPU);

   if sCheckLPYou.Checked then
   begin
    bufLPYOU := StrToInt(sCheckLPYou.Caption);
    WriteProcessMemory(hProcess,ptr(GetEmule($B419A4)), Addr(bufLPYOU),2,temp);
   end;

   ReadProcessMemory(hProcess,ptr(GetEmule($B419A4)), Addr(bufLPYOU),2,temp);
   sCheckLPYou.Caption := IntToStr(bufLPYOU);
  end;

  if bufIsDuel = 1 then
  begin
    if (Config.IsRankPreview) and (not bGanhaSTEC) then
       sStatusBar1.Panels[3].Text := calcular(hProcess);

    ReadProcessMemory(hProcess,ptr(GetEmule($AF2D01)), Addr(isNPC),1,temp);

     if (not bFinalView) and (not bTurnAnel) then
     if sStatusBar1.Tag > 0 then
       sStatusBar1.Tag := sStatusBar1.Tag - (10 * TIProcess.Interval div 100) else
     begin
       sStatusBar1.Panels.Items[0].Text := NPC[isNPC];
       sStatusBar1.Tag := 0;
     end;

    if Config.IsReliquiasOn then
    begin
      // Se Existe Carta Principal
      ReadProcessMemory(hProcess,ptr(GetEmule(Codes.Is1)+11), Addr(isCard[1]),1,temp);
      ReadProcessMemory(hProcess,ptr(GetEmule(Codes.Is2)+11), Addr(isCard[2]),1,temp);
      ReadProcessMemory(hProcess,ptr(GetEmule(Codes.Is3)+11), Addr(isCard[3]),1,temp);
      ReadProcessMemory(hProcess,ptr(GetEmule(Codes.Is4)+11), Addr(isCard[4]),1,temp);
      ReadProcessMemory(hProcess,ptr(GetEmule(Codes.Is5)+11), Addr(isCard[5]),1,temp);
      ReadProcessMemory(hProcess,ptr(GetEmule(Codes.Is6)+11), Addr(isCard[6]),1,temp);
      ReadProcessMemory(hProcess,ptr(GetEmule(Codes.Is7)+11), Addr(isCard[7]),1,temp);
      ReadProcessMemory(hProcess,ptr(GetEmule(Codes.Is8)+11), Addr(isCard[8]),1,temp);
      ReadProcessMemory(hProcess,ptr(GetEmule(Codes.Is9)+11), Addr(isCard[9]),1,temp);
      ReadProcessMemory(hProcess,ptr(GetEmule(Codes.Is10)+11), Addr(isCard[10]),1,temp);
      // Se Existe Carta Secund�rio
      ReadProcessMemory(hProcess,ptr(GetEmule(CodesF.Is1)+11), Addr(isCardF[1]),1,temp);
      ReadProcessMemory(hProcess,ptr(GetEmule(CodesF.Is2)+11), Addr(isCardF[2]),1,temp);
      ReadProcessMemory(hProcess,ptr(GetEmule(CodesF.Is3)+11), Addr(isCardF[3]),1,temp);
      ReadProcessMemory(hProcess,ptr(GetEmule(CodesF.Is4)+11), Addr(isCardF[4]),1,temp);
      ReadProcessMemory(hProcess,ptr(GetEmule(CodesF.Is5)+11), Addr(isCardF[5]),1,temp);
      ReadProcessMemory(hProcess,ptr(GetEmule(CodesF.Is6)+11), Addr(isCardF[6]),1,temp);
      ReadProcessMemory(hProcess,ptr(GetEmule(CodesF.Is7)+11), Addr(isCardF[7]),1,temp);
      ReadProcessMemory(hProcess,ptr(GetEmule(CodesF.Is8)+11), Addr(isCardF[8]),1,temp);
      ReadProcessMemory(hProcess,ptr(GetEmule(CodesF.Is9)+11), Addr(isCardF[9]),1,temp);
      ReadProcessMemory(hProcess,ptr(GetEmule(CodesF.Is10)+11), Addr(isCardF[10]),1,temp);
      // ID das Cartas Principal
      ReadProcessMemory(hProcess,ptr(GetEmule(Codes.Is1)), Addr(bufID[1]),2,temp);
      ReadProcessMemory(hProcess,ptr(GetEmule(Codes.Is2)), Addr(bufID[2]),2,temp);
      ReadProcessMemory(hProcess,ptr(GetEmule(Codes.Is3)), Addr(bufID[3]),2,temp);
      ReadProcessMemory(hProcess,ptr(GetEmule(Codes.Is4)), Addr(bufID[4]),2,temp);
      ReadProcessMemory(hProcess,ptr(GetEmule(Codes.Is5)), Addr(bufID[5]),2,temp);
      ReadProcessMemory(hProcess,ptr(GetEmule(Codes.Is6)), Addr(bufID[6]),2,temp);
      ReadProcessMemory(hProcess,ptr(GetEmule(Codes.Is7)), Addr(bufID[7]),2,temp);
      ReadProcessMemory(hProcess,ptr(GetEmule(Codes.Is8)), Addr(bufID[8]),2,temp);
      ReadProcessMemory(hProcess,ptr(GetEmule(Codes.Is9)), Addr(bufID[9]),2,temp);
      ReadProcessMemory(hProcess,ptr(GetEmule(Codes.Is10)), Addr(bufID[10]),2,temp);
      // ID das Cartas Secund�rio
      ReadProcessMemory(hProcess,ptr(GetEmule(CodesF.Is1)), Addr(bufIDF[1]),2,temp);
      ReadProcessMemory(hProcess,ptr(GetEmule(CodesF.Is2)), Addr(bufIDF[2]),2,temp);
      ReadProcessMemory(hProcess,ptr(GetEmule(CodesF.Is3)), Addr(bufIDF[3]),2,temp);
      ReadProcessMemory(hProcess,ptr(GetEmule(CodesF.Is4)), Addr(bufIDF[4]),2,temp);
      ReadProcessMemory(hProcess,ptr(GetEmule(CodesF.Is5)), Addr(bufIDF[5]),2,temp);
      ReadProcessMemory(hProcess,ptr(GetEmule(CodesF.Is6)), Addr(bufIDF[6]),2,temp);
      ReadProcessMemory(hProcess,ptr(GetEmule(CodesF.Is7)), Addr(bufIDF[7]),2,temp);
      ReadProcessMemory(hProcess,ptr(GetEmule(CodesF.Is8)), Addr(bufIDF[8]),2,temp);
      ReadProcessMemory(hProcess,ptr(GetEmule(CodesF.Is9)), Addr(bufIDF[9]),2,temp);
      ReadProcessMemory(hProcess,ptr(GetEmule(CodesF.Is10)), Addr(bufIDF[10]),2,temp);

      // ############ 1
      if isCard[1] > 0 then
      begin
        lbID1.Caption := inttostr(bufID[1]);
        alterarSimbolo(bufID[1],lbSymbol1);
        ReadProcessMemory(hProcess,ptr(GetEmule(Codes.Is1)+2), Addr(bufATK),2,temp);
        ReadProcessMemory(hProcess,ptr(GetEmule(Codes.Is1)+4), Addr(bufDEF),2,temp);
        ReadProcessMemory(hProcess,ptr(GetEmule(Codes.Is1)+6), Addr(bufEQP),2,temp);
        ReadProcessMemory(hProcess,ptr(GetEmule(Codes.Is1)+8), Addr(bufBIO),2,temp);
        alterarInfoATKDEF(isCard[1],bufATK,bufDEF,bufEQP,bufBIO,lbATK1,lbDEF1);
        abrirImagem(bufID[1],img01,Config.IsViewMonster = 2,StatusCardIsATKDEF(isCard[1]), isCard[1]);
      end else
      begin
        alterarInfoATKDEF(0,0,0,0,0,lbATK1,lbDEF1);
        lbID1.Caption := '0';
        lbSymbol1.Caption := '';
        img01.Picture := nil;
      end;

      // ############ 2
      if isCard[2] > 0 then
      begin
        lbID2.Caption := inttostr(bufID[2]);
        alterarSimbolo(bufID[2],lbSymbol2);
        ReadProcessMemory(hProcess,ptr(GetEmule(Codes.Is2)+2), Addr(bufATK),2,temp);
        ReadProcessMemory(hProcess,ptr(GetEmule(Codes.Is2)+4), Addr(bufDEF),2,temp);
        ReadProcessMemory(hProcess,ptr(GetEmule(Codes.Is2)+6), Addr(bufEQP),2,temp);
        ReadProcessMemory(hProcess,ptr(GetEmule(Codes.Is2)+8), Addr(bufBIO),2,temp);
        alterarInfoATKDEF(isCard[2],bufATK,bufDEF,bufEQP,bufBIO,lbATK2,lbDEF2);
        abrirImagem(bufID[2],img02,Config.IsViewMonster = 2,StatusCardIsATKDEF(isCard[2]), isCard[2]);
      end else
      begin
        alterarInfoATKDEF(0,0,0,0,0,lbATK2,lbDEF2);
        lbID2.Caption := '0';
        lbSymbol2.Caption := '';
        img02.Picture := nil;
      end;
     
      // ############ 3
      if isCard[3] > 0 then
      begin
        lbID3.Caption := inttostr(bufID[3]);
        alterarSimbolo(bufID[3],lbSymbol3);
        ReadProcessMemory(hProcess,ptr(GetEmule(Codes.Is3)+2), Addr(bufATK),2,temp);
        ReadProcessMemory(hProcess,ptr(GetEmule(Codes.Is3)+4), Addr(bufDEF),2,temp);
        ReadProcessMemory(hProcess,ptr(GetEmule(Codes.Is3)+6), Addr(bufEQP),2,temp);
        ReadProcessMemory(hProcess,ptr(GetEmule(Codes.Is3)+8), Addr(bufBIO),2,temp);
        alterarInfoATKDEF(isCard[3],bufATK,bufDEF,bufEQP,bufBIO,lbATK3,lbDEF3);
        abrirImagem(bufID[3],img03,Config.IsViewMonster = 2,StatusCardIsATKDEF(isCard[3]), isCard[3]);
      end else
      begin
        alterarInfoATKDEF(0,0,0,0,0,lbATK3,lbDEF3);
        lbID3.Caption := '0';
        lbSymbol3.Caption := '';
        img03.Picture := nil;
      end;
     
      // ############ 4
      if isCard[4] > 0 then
      begin
        lbID4.Caption := inttostr(bufID[4]);
        alterarSimbolo(bufID[4],lbSymbol4);
        ReadProcessMemory(hProcess,ptr(GetEmule(Codes.Is4)+2), Addr(bufATK),2,temp);
        ReadProcessMemory(hProcess,ptr(GetEmule(Codes.Is4)+4), Addr(bufDEF),2,temp);
        ReadProcessMemory(hProcess,ptr(GetEmule(Codes.Is4)+6), Addr(bufEQP),2,temp);
        ReadProcessMemory(hProcess,ptr(GetEmule(Codes.Is4)+8), Addr(bufBIO),2,temp);
        alterarInfoATKDEF(isCard[4],bufATK,bufDEF,bufEQP,bufBIO,lbATK4,lbDEF4);
        abrirImagem(bufID[4],img04,Config.IsViewMonster = 2,StatusCardIsATKDEF(isCard[4]), isCard[4]);
      end else
      begin
        alterarInfoATKDEF(0,0,0,0,0,lbATK4,lbDEF4);
        lbID4.Caption := '0';
        lbSymbol4.Caption := '';
        img04.Picture := nil;
      end;

      // ############ 5
      if isCard[5] > 0 then
      begin
        lbID5.Caption := inttostr(bufID[5]);
        alterarSimbolo(bufID[5],lbSymbol5);
        ReadProcessMemory(hProcess,ptr(GetEmule(Codes.Is5)+2), Addr(bufATK),2,temp);
        ReadProcessMemory(hProcess,ptr(GetEmule(Codes.Is5)+4), Addr(bufDEF),2,temp);
        ReadProcessMemory(hProcess,ptr(GetEmule(Codes.Is5)+6), Addr(bufEQP),2,temp);
        ReadProcessMemory(hProcess,ptr(GetEmule(Codes.Is5)+8), Addr(bufBIO),2,temp);
        alterarInfoATKDEF(isCard[5],bufATK,bufDEF,bufEQP,bufBIO,lbATK5,lbDEF5);
        abrirImagem(bufID[5],img05,Config.IsViewMonster = 2,StatusCardIsATKDEF(isCard[5]), isCard[5]);
      end else
      begin
        alterarInfoATKDEF(0,0,0,0,0,lbATK5,lbDEF5);
        lbID5.Caption := '0';
        lbSymbol5.Caption := '';
        img05.Picture := nil;
      end;

      // ############ 6
      if isCard[6] = 0 then
         bufID[6] := 0;
      lbID6.Caption := inttostr(bufID[6]);
      alterarTipo(bufID[6],lbTP6);
      abrirImagem(bufID[6],img06,Config.IsViewOther = 2, 1, isCard[6]);

      // ############ 7
      if isCard[7] = 0 then
         bufID[7] := 0;
      lbID7.Caption := inttostr(bufID[7]);
      alterarTipo(bufID[7],lbTP7);
      abrirImagem(bufID[7],img07,Config.IsViewOther = 2, 1, isCard[7]);

      // ############ 8
      if isCard[8] = 0 then
         bufID[8] := 0;
      lbID8.Caption := inttostr(bufID[8]);
      alterarTipo(bufID[8],lbTP8);
      abrirImagem(bufID[8],img08,Config.IsViewOther = 2, 1, isCard[8]);

      // ############ 9
      if isCard[9] = 0 then
         bufID[9] := 0;
      lbID9.Caption := inttostr(bufID[9]);
      alterarTipo(bufID[9],lbTP9);
      abrirImagem(bufID[9],img09,Config.IsViewOther = 2, 1, isCard[9]);

      // ############ 10
      if isCard[10] = 0 then
         bufID[10] := 0;
      lbID10.Caption := inttostr(bufID[10]);
      alterarTipo(bufID[10],lbTP10);
      abrirImagem(bufID[10],img10,Config.IsViewOther = 2, 1, isCard[10]);

      ReadProcessMemory(hProcess,ptr(GetEmule(AD_SUBSTITUIR)), Addr(isSubstituir),1,temp);
      // Fun��es do Enigma e Anel...
      case Config.IsReliquiasGet of
       0: begin
           isSubstituir := 0;
           WriteProcessMemory(hProcess,ptr(GetEmule(AD_CARD_OLHO)), Addr(isSubstituir),1,temp);
          end;
       1: begin
        for i:= 1 to 5 do
        begin 
          if isCard[i] > 0 then
          begin
            for l:= 0 to Length(Enigma)-1 do
            if bufID[i] = Enigma[l].ID then
            if (Enigma[l].sATc = 0) or (Enigma[l].sATc = i) then
            begin
             if ((Enigma[l].Tipo = 0) or (Enigma[l].Tipo = 11)) and
                IsSubstituirCard(isCard[i],isCardF) and
                (isSubstituir = 55) then
             Enigma[l].sAT := False;
             TipoBonusReliquia(Enigma[l],bufID,bufIDF,isCard,isCardF,hProcess);
            end;
          end else
          for l:= 0 to Length(Enigma)-1 do
          if ((bufID[i] = Enigma[l].ID) and (Enigma[l].Tipo <> 0)) or
             (((Enigma[l].Tipo = 0)or(Enigma[l].Tipo = 11)) and (i = Enigma[l].sATc)) then
          begin
           Enigma[l].sAT := False;
           TipoBonusReliquia(Enigma[l],bufID,bufIDF,isCard,isCardF,hProcess);
          end;
        end;
       end;
       2: begin
           for i:= 1 to 5 do
           begin
            if isCard[i] > 0 then
            begin
              for l:= 0 to Length(Anel)-1 do
              if bufID[i] = Anel[l].ID then
              begin
                if (Anel[l].sATc = 0) or (Anel[l].sATc = i) then
                begin
                 if ((Anel[l].Tipo = 0) or (Anel[l].Tipo = 11)) and
                    IsSubstituirCard(isCard[i],isCardF) and
                    (isSubstituir = 55) then
                 Anel[l].sAT := False;
                 TipoBonusReliquia(Anel[l],bufID,bufIDF,isCard,isCardF,hProcess);
                end;
              end else
              if Anel[l].ID = 0 then
              begin
                 TipoBonusReliquia(Anel[l],bufID,bufIDF,isCard,isCardF,hProcess);
              end;
            end else
            for l:= 0 to Length(Anel)-1 do
            if ((bufID[i] = Anel[l].ID) and (Anel[l].Tipo <> 0)) or
               (((Anel[l].Tipo = 0)or(Anel[l].Tipo = 11)) and (i = Anel[l].sATc)) then
            begin
             Anel[l].sAT := False;
             TipoBonusReliquia(Anel[l],bufID,bufIDF,isCard,isCardF,hProcess);
            end;
           end;
           
           ReadProcessMemory(hProcess,ptr(GetEmule(AD_TURN)), Addr(bufIsTurno),1,temp);
           if (bufIsTurno <> iTurnAnel) then
           begin
             iTurnAnel := bufIsTurno;
             bTurnAnel := True;
           end else
           begin
            if bTurnAnel then
            begin
             bTurnAnel := False;
             Randomize;
             if (RandomRange(1,15) = 5) and (bufIsTurno > 0) then
             begin
              ReadProcessMemory(hProcess,ptr(GetEmule($B419C4)), Addr(bufLPCPU),2,temp);
              bufLPCPU := bufLPCPU - (Trunc(bufLPCPU * 0.15));
              WriteProcessMemory(hProcess,ptr(GetEmule($B419C4)), Addr(bufLPCPU),2,temp);
              sStatusBar1.Panels.Items[0].Text := '15% de LP Drenado do Inimigo!';
              sStatusBar1.Tag := Trunc(ABS(TIProcess.Interval - 1600) * 0.2);
              reproduzirAudio(2);
             end;
            end;
           end;

           bufBiome := 6;
           selecionarBioma(6);
           WriteProcessMemory(hProcess,ptr(GetEmule(AD_BIOMA)), Addr(bufBiome),1,temp);
         end;
      end;
    end;

    ReadProcessMemory(hProcess,ptr(GetEmule(AD_BIOMA)), Addr(bufBiome),1,temp);
    if selectBiome <> bufBiome then
    begin
      selectBiome := bufBiome;
      selecionarBioma(bufBiome);
    end;

    ReadProcessMemory(hProcess,ptr(GetEmule(AD_TURN)), Addr(bufIsTurno),1,temp);
    if (bufIsTurno = 0) then
       sStatusBar1.Panels.Items[0].Text := NPC[isNPC];    
    lbTurno.Caption := 'Turno: '+IntToStr(bufIsTurno);
    ReadProcessMemory(hProcess,ptr(GetEmule(AD_TURN)-1), Addr(isENDVit),1,temp);
    ReadProcessMemory(hProcess,ptr(GetEmule(AD_STAR_Victory)), Addr(isEND),1,temp);
    //ReadProcessMemory(hProcess,ptr(StrToInt('$' + AD_END_POS)), Addr(IsATT1),1,temp);
    if (isENDVit = 2) then
    if Config.IsReliquiasGet = 1 then
    if not bFinalEnigma then
     begin
       bFinalEnigma := True;
       Randomize;
       if RandomRange(1,10) = 5 then
       begin
          bGanhaSTEC := True;
          sStatusBar1.Panels[3].Text := '*S TEC';
          isENDVit := 132;
          WriteProcessMemory(hProcess,ptr(GetEmule($B41990)), Addr(isENDVit),1,temp);
          Sleep(45);
          WriteProcessMemory(hProcess,ptr(GetEmule($B41990)), Addr(isENDVit),1,temp);
          Sleep(45);
          WriteProcessMemory(hProcess,ptr(GetEmule($B41990)), Addr(isENDVit),1,temp);
          reproduzirAudio(3);
       end;
     end;
     
    if (isENDVit > 0) and (isEND > 0) and (isEND < 6) then
    begin
     if Config.isDropMOD then
     if bDrop = False then
     begin
      if Dropar(isNPC,qntDuelos(isNPC,hProcess),calcular(hProcess),gCard) then
      begin
        WriteProcessMemory(hProcess,ptr(GetEmule(AD_END_Receiv[1])), Addr(gCard),2,temp);
        WriteProcessMemory(hProcess,ptr(GetEmule(AD_END_Receiv[2])), Addr(gCard),2,temp);
        WriteProcessMemory(hProcess,ptr(GetEmule(AD_END_Receiv[3])), Addr(gCard),2,temp);
        bDropY := True;
        IsGeneric := 32;
        WriteProcessMemory(hProcess,ptr(GetEmule(AD_END_MOVE)), Addr(IsGeneric),1,temp);
        Sleep(45);
        WriteProcessMemory(hProcess,ptr(GetEmule(AD_END_MOVE)), Addr(IsGeneric),1,temp);
        Sleep(45);
        WriteProcessMemory(hProcess,ptr(GetEmule(AD_END_MOVE)), Addr(IsGeneric),1,temp);
      end;
      bDrop := True;
     end;

       if Config.IsStarReceiv then
       begin
         if bStar = False then
         begin
           receivStar := Estrelas(calcular(hProcess));
           sStatusBar1.Panels.Items[2].Text := 'Ganho de Estrelas: ' + IntToStr(isEND + receivStar);
           starCount := starCount + receivStar;
           bStar := True;
           WriteProcessMemory(hProcess,ptr(GetEmule(AD_STAR_NUM)), Addr(starCount),4,temp);
           notMegaMorph(hProcess);
         end;
       end else
           sStatusBar1.Panels.Items[2].Text := 'Ganho de Estrelas: ' + IntToStr(isEND);

       s := '';
       if bDropY then
       begin
         s := '*';
         lbDescBiome.Caption := 'MOD';
       end;

       bFinalView := True;
       ReadProcessMemory(hProcess,ptr(GetEmule(AD_END_Receiv[3])), Addr(isEND_Card),2,temp);
       imgNPC.Proportional := True;
       imgNPC.Cursor := crHandPoint;
       imgNPC.Tag := isEND_Card;
       visualizarImagem(isEND_Card, imgNPC);
       sStatusBar1.Panels.Items[0].Text := s+FormatFloat('000',isEND_Card) + ' ' +
       bkListView.FindCaption(0,IntToStr(isEND_Card),True,True,True).SubItems[0];
    end else
    begin
      if (isNPC <= 0) or (isNPC >= 40) then
      begin
        if (Config.IsReliquiasOn) and  (Config.IsReliquiasGet = 1) then
         isNPC := 40
         else isNPC := 0;
      end;
       imgNPC.Picture := nil;
       ilImgNPC.GetBitmap(isNPC,imgNPC.Picture.Bitmap);
       imgNPC.Proportional := False;
       imgNPC.Refresh;
       ReadProcessMemory(hProcess,ptr(GetEmule($B419C4)), Addr(bufLPCPU),2,temp);
       ReadProcessMemory(hProcess,ptr(GetEmule($B419A4)), Addr(bufLPYOU),2,temp);
       sStatusBar1.Panels.Items[2].Text := 'LP > YOU: ' + IntToStr(bufLPYOU) + ' | COM: ' + IntToStr(bufLPCPU);
    end;
  end else
  begin
   restaurarDescricoes;
   selecionarBioma(0);
   lbTurno.Caption := '';
   bufIsDuel := 0;
   imgNPC.Picture := nil;
   bStar := False;
   bDrop := False;
   bDropY := False;
   if Config.IsReliquiasGet = 1 then
   begin
    for l:= 0 to Length(Enigma)-1 do
     Enigma[l].sAT := False;
   end else
   begin
    for l:= 0 to Length(Anel)-1 do
     Anel[l].sAT := False;
   end;
   sStatusBar1.Panels.Items[0].Text := 'YuGiOh Forbidden Memories';
   sStatusBar1.Panels.Items[2].Text := 'Estrelas Ganha: 0';
   sStatusBar1.Panels.Items[3].Text := 'PREVER RANK';
   bFinalView := False;
   bFinalEnigma := False;
   bGanhaSTEC := False;
   bTurnAnel := True;
   iTurnAnel := bufIsTurno;
   imgNPC.Proportional := False;
   imgNPC.Cursor := crDefault;
   imgNPC.Tag := 0;
   if (Config.IsReliquiasOn) and (Config.IsReliquiasGet = 1) then
    isNPC := 40
   else isNPC := 0;
   ilImgNPC.GetBitmap(isNPC,imgNPC.Picture.Bitmap);
   WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is1)), Addr(bufIsDuel),10,temp);
   WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is2)), Addr(bufIsDuel),10,temp);
   WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is3)), Addr(bufIsDuel),10,temp);
   WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is4)), Addr(bufIsDuel),10,temp);
   WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is5)), Addr(bufIsDuel),10,temp);
   WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is6)), Addr(bufIsDuel),2,temp);
   WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is7)), Addr(bufIsDuel),2,temp);
   WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is8)), Addr(bufIsDuel),2,temp);
   WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is9)), Addr(bufIsDuel),2,temp);
   WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is10)), Addr(bufIsDuel),2,temp);
  end;

  ReadProcessMemory(hProcess,ptr(GetEmule(AD_PASS_IsFORM[1])), Addr(isPASS_Form1),1,temp);
  ReadProcessMemory(hProcess,ptr(GetEmule(AD_PASS_IsFORM[2])), Addr(isPASS_Form2),1,temp);
  ReadProcessMemory(hProcess,ptr(GetEmule(AD_PASS_IsFORM[3])), Addr(isPASS_Form3),1,temp);

  if (isPASS_Form1 = 248) and (isPASS_Form2 = 248) and (isPASS_Form3 = 200) then
  begin
    sStatusBar1.Panels.Items[0].Text := 'YuGiOh PassWord';
    if Config.IsYesPassword then
    begin
      for i:= 1 to 8 do  // verificar pressionar
      ReadProcessMemory(hProcess,ptr(GetEmule(AD_PASS_NUM[i])), Addr(PassNumber[i]),1,temp);
      ReadProcessMemory(hProcess,ptr(GetEmule(AD_END_MOVE)-5), Addr(IsGeneric),1,temp);
      ReadProcessMemory(hProcess,ptr(GetEmule(AD_END_MOVE)-5), Addr(IsGeneric),1,temp);
      //ReadProcessMemory(hProcess,ptr(GetEmule(AD_PASS_IsSHOP)), Addr(IsShop),1,temp);
      IsShop := 0; // talvez ser� removido // TODO StandBye
      ReadProcessMemory(hProcess,ptr(GetEmule(AD_PASS_SHOW_CARD)), Addr(IsViraCard),1,temp);
      if (IsGeneric = 64) and (IsShop = 0) and (IsViraCard = 0) then
      begin
        s := '';
        for i:= 1 to 8 do
        s := s + IntToStr(PassNumber[i]);
        for i:=0 to Length(Passwords) do
        if FormatFloat('00000000',Passwords[i][1]) = s then
        begin
         WriteProcessMemory(hProcess,ptr(GetEmule(AD_PASS)), Addr(Passwords[i][0]),2,temp);
         IsGeneric := 1;
         WriteProcessMemory(hProcess,ptr(GetEmule(AD_PASS_SHOW_CARD)), Addr(IsGeneric),1,temp);
        end;
      end;
    end;

   if Config.IsNotLimitedCard then
    begin
     IsGeneric := 0;
     for i:= 0 to 90 do
     WriteProcessMemory(hProcess,ptr(GetEmule(AD_PASS_REPEAT)+i), Addr(IsGeneric),1,temp);
     notMegaMorph(hProcess);
    end;
  end else
  if (isPASS_Form1 = 240) and (isPASS_Form2 = 240) and (isPASS_Form3 = 192) then
  begin
   sStatusBar1.Panels.Items[0].Text := 'YuGiOh My Deck';
   if Config.IsReliquiasGet = 2 then
   begin
    bufBiome := 6;
    WriteProcessMemory(hProcess,ptr(GetEmule(AD_BIOMA)), Addr(bufBiome),1,temp);
   end;
  end;

  //freemem(buf);
  if hProcess <> 0 then
    CloseHandle(hProcess);
 end else
  btAT.Click;
end;

procedure TForm1.btRunClick(Sender: TObject);
begin
 Config.wEmu := cbEmulador.ItemIndex;

 if Emuladores[Config.wEmu].SunNumber[1] in['P','N'] then
 begin
  try
   StrToInt(Copy(Emuladores[Config.wEmu].SunNumber,2,Length(Emuladores[Config.wEmu].SunNumber)));
  except
   ShowMessage('Valor Inv�lido da defini��o do Emulador!');
   Exit;
  end;
 end else
 begin
   ShowMessage('Valor de defini��o Positivo ou Negativo do Emulador � Inv�lido!');
   Exit;
 end;

 reproduzirAudio; 
 sPanelRun.Visible := False;
 TIProcess.Enabled := True;

 if (Config.IsReliquiasOn) then
 begin
  if Config.IsReliquiaGifMove then
    img1.Picture := cxImgColecaoAtivadoGif.Items[Config.IsReliquiasGet].Picture
  else img1.Picture := cxImgColecaoAtivado.Items[Config.IsReliquiasGet].Picture;
 end else img1.Picture := cxImgColecaoAtivado.Items[4].Picture;
 btAT.Caption := 'Desativar';
 sSkinManager1.RepaintForms;
end;

procedure TForm1.abrirImagem(ID: Integer; var IMG: TImage; IsAlpha: Boolean; const isDEF: Byte = 0; const inUse: Integer = 0);
var
  dir: string;
  // notImgDef: Boolean;
  imgTmp: TImage;
  bmp: TBitmap;
  DC: longint;
  H, W : integer;
begin
 if (IMG.Visible and not Assigned(IMG.Picture) and (ID > 0)) or
    (IMG.Visible and ((isDEF <> IMG.Tag) or (ID <> IMG.HelpContext)) and (ID > 0)) then
 begin
  // notImgDef := (not Config.IsModDEF) or (isDEF <> 2);
  IMG.Tag := isDEF;
  IMG.HelpContext := ID;

  try
   imgTmp := TImage.Create(nil);
   visualizarImagem(ID, imgTmp, Config.IsPeB);
   bmp := TBitmap.Create;
   bmp.Assign(imgTmp.Picture.Graphic);
   if (Config.IsModDEF) and (isDEF = 2) then
   begin
     DC := bmp.Canvas.Handle;
     H := bmp.Height;
     W := bmp.Width;
     RotateBitmap(DC, W, H, PI * -90 / 180);
     IMG.Picture.Bitmap.Width := W;
     IMG.Picture.Bitmap.Height := H;
     BitBlt(IMG.Picture.Bitmap.Canvas.Handle, 0, 0, W, H, DC, 0, 0, SRCCopy);
     IMG.Transparent := True;
   end else IMG.Picture.Bitmap.Assign(bmp);
   if IsAlpha and Config.IsOpaque and Assigned(IMG.Picture.Graphic) then
   begin
     if Config.wSkin = 2 then
      OpacidadeIMG(IMG.Picture.Bitmap,10) else
      OpacidadeIMG(IMG.Picture.Bitmap,128);
   end;
   IMG.Refresh;
   bmp.Free;
   FreeAndNil(imgTmp);
  except
   IMG.Picture := nil;
   FreeAndNil(imgTmp);
  end;
 end else
 if ID = 0 then
   IMG.Picture := nil;
end;

procedure TForm1.blMinimizeClick(Sender: TObject);
Begin
  Form1.AutoSize := False;
  Form1.Height := sPanelBG.Height + 5;
  ShowWindow(Handle,SW_MINIMIZE);
end;

procedure TForm1.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
    with Params do begin
     ExStyle := ExStyle or WS_EX_TOPMOST or WS_EX_OVERLAPPEDWINDOW;
     WndParent := GetDesktopwindow;
    end;
end;

function TForm1.verificarSmallInt(X: Integer):Integer;
begin
 if X > 32766 then
  Result :=  - 65536 + X else Result := X;
end;

procedure TForm1.selecionarBioma(X: Integer);
begin
  imgBiome.Picture := nil;

  case X of
    0..6: begin
           ilImgBiome.GetBitmap(X, imgBiome.Picture.Bitmap);
           lbDescBiome.Caption := Biome[X];
          end;
  else ilImgBiome.GetBitmap(0, imgBiome.Picture.Bitmap);
       lbDescBiome.Caption := 'Nenhum';
  end;
end;

procedure TForm1.alterarInfoATKDEF(isCard, ATK, DEF, EQP, BIO: Smallint; var lbATK,
  lbDEF: TsLabel);
const
  sDesc: array[0..3] of string = ('',' E',' B',' EB');
var
 sALT: Byte;
 sTotal: Integer;
begin
  sALT := 0;
  EQP := verificarSmallInt(EQP);
  BIO := verificarSmallInt(BIO);

  if (EQP <> 0) and (BIO <> 0) then
      sALT := 3 else
  if (EQP = 0) and (BIO <> 0) then
      sALT := 2 else
  if (EQP <> 0) and (BIO = 0) then
      sALT := 1;

  sTotal:= ATK + DEF;

  ATK := ATK + EQP + BIO;
  DEF := DEF + EQP + BIO;
  lbATK.Caption := IntToStr(ATK) + sDesc[sALT];
  lbDEF.Caption := IntToStr(DEF) + sDesc[sALT];

  if sTotal > ATK+DEF then
  begin
   lbATK.Font.Color := clMaroon;
   lbDEF.Font.Color := clMaroon;
  end else
  if sTotal < ATK+DEF then
  begin
   lbATK.Font.Color := clBlue;
   lbDEF.Font.Color := clBlue;
  end else
  if Config.wSkin = 2 then
  begin
   lbATK.Font.Color := clWhite;
   lbDEF.Font.Color := clWhite;
  end else
  begin
   lbATK.Font.Color := clBlack;
   lbDEF.Font.Color := clBlack;
  end;

  case StatusCardIsATKDEF(isCard) of
   0:
   begin
    lbATK.Font.Style := [];
    lbDEF.Font.Style := [];
   end;
   1:
   begin
    lbATK.Font.Style := [fsBold];
    lbDEF.Font.Style := [];
   end;
   2:
   begin
    lbATK.Font.Style := [];
    lbDEF.Font.Style := [fsBold];
   end;
  end;

end;

procedure TForm1.alterarSimbolo(ID: Word; var lbSimbol: TsLabel);
var
 Search:TlistItem;
 Sb: Byte;
 sResult: string;
begin
 if Config.IsReliquiasGet = 0 then
 begin
   Search := bkListView.FindCaption(0,IntToStr(ID),True,True,True);
   if Search <> nil then
   begin
     Sb := AnsiIndexText(Search.SubItems.Strings[3],Simbolo);
     sResult := Symbol[Sb+1] + ' ' +
     Symbol[StrtoInt('$'+SymbolADV[Sb][1])+1] + ' ' +
     Symbol[StrtoInt('$'+SymbolADV[Sb][2])+1];
     lbSimbol.Caption := sResult;
   end;
 end else
    lbSimbol.Caption := '';
end;

procedure TForm1.restaurarDescricoes;
var
  i: Integer;
begin
  for i:= 0 to sPanelM.ControlCount-1 do
  begin
    if (sPanelM.Controls[i] is TImage) then
       (sPanelM.Controls[i] as TImage).Picture := nil;
    if (sPanelM.Controls[i] is TsLabel) then
       (sPanelM.Controls[i] as TsLabel).Caption := '';
  end;
  for i:= 0 to sPanelO.ControlCount-1 do
  begin
    if (sPanelO.Controls[i] is TImage) then
       (sPanelO.Controls[i] as TImage).Picture := nil;
    if (sPanelO.Controls[i] is TsLabel) then
       (sPanelO.Controls[i] as TsLabel).Caption := '';
  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
const
  s1 = 'Rel�quias do Mil�nio - ';
  s2 = 'By ';
  s3 = 'Je';
  s4 = 'Ph';
  s5 = 'eR';
var
  Stream: TStream;
begin
  Config.escalaPos := 0;
  selectBiome := 0;
  //DoubleBuffered := True;
  isGoPID := 'ePSXe';
  Form1.Caption := s1+s2+s3+s4+s5;
  Application.Title := s1+s2+s3+s4+s5+#9+'v1.0.3.7';
  dataset := TStringList.Create;
  Stream := TResourceStream.Create(hInstance, 'DBA', RT_RCDATA);
  if (Assigned(Stream)) then
  dataset.LoadFromStream(Stream);
end;

procedure TForm1.btDBClick(Sender: TObject);
begin
 if FormDB.Showing and (FormDB.WindowState <> wsMinimized) then
    FormDB.Close else
 begin
   if (FormDB.WindowState = wsMinimized) then
      ShowWindow(FormDB.Handle, SW_RESTORE);
    FormDB.Show;
    //sSkinManager1.RepaintForms;
 end;
end;

function TForm1.calcularResultado(MyLP: Word; Vitory, Turn, EffectATK,
  DefWin, DownFace, Fusoes, Equips, Magias, Traps, Combo, UseCards: Byte): String;
var
 calculo: ShortInt;
begin
 calculo:= 50;
  case Turn of
    0..4: calculo := calculo + 12;
    5..8: calculo := calculo + 8;
    9..28: calculo := calculo + 0;
    29..32: calculo := calculo - 8;
    33..255: calculo := calculo - 12;
  end;
  case EffectATK of
    0..1: calculo := calculo + 4;
    2..3: calculo := calculo + 2;
    4..9: calculo := calculo + 0;
    10..19: calculo := calculo - 2;
    20..255: calculo := calculo - 4;
  end;
  case DefWin of
    0..2: calculo := calculo + 0;
    3..6: calculo := calculo - 10;
    7..10: calculo := calculo - 20;
    11..15: calculo := calculo - 30;
    16..255: calculo := calculo - 40;
  end;
  case DownFace of
    0: calculo := calculo + 0;
    1..10: calculo := calculo - 2;
    11..20: calculo := calculo - 4;
    21..30: calculo := calculo - 6;
    31..255: calculo := calculo - 8;
  end;
  case Fusoes of
    0: calculo := calculo + 4;
    1..4: calculo := calculo + 0;
    5..9: calculo := calculo - 4;
    10..14: calculo := calculo - 8;
    15..255: calculo := calculo - 12;
  end;
  case Equips of
    0: calculo := calculo + 4;
    1..4: calculo := calculo + 0;
    5..9: calculo := calculo - 4;
    10..14: calculo := calculo - 8;
    15..255: calculo := calculo - 12;
  end;
  case Magias of
    0: calculo := calculo + 2;
    1..3: calculo := calculo - 4;
    4..6: calculo := calculo - 8;
    7..9: calculo := calculo - 12;
    10..255: calculo := calculo - 16;
  end;
  case Traps of
    0: calculo := calculo + 2;
    1..2: calculo := calculo - 8;
    3..4: calculo := calculo - 16;
    5..6: calculo := calculo - 24;
    7..255: calculo := calculo - 32;
  end;
  case UseCards of
    0..8: calculo := calculo + 15;
    9..12: calculo := calculo + 12;
    13..32: calculo := calculo + 0;
    33..36: calculo := calculo - 5;
    37..255: calculo := calculo - 7;
  end;
  case MyLP of
    0..99: calculo := calculo - 7;
    100..999: calculo := calculo - 5;
    1000..6999: calculo := calculo + 0;
    7000..7999: calculo := calculo + 4;
    8000..65535: calculo := calculo + 6;
  end;
  case Vitory of
    40: calculo := calculo + 40;
    216: calculo := calculo - 40;
  else ;  calculo := calculo + 2; end;
  case calculo of
    -128..9: Result:= 'S TEC';
    10..19: Result:= 'A TEC';
    20..29: Result:= 'B TEC';
    30..39: Result:= 'C TEC';
    40..49: Result:= 'D TEC';
    50..59: Result:= 'D POW';
    60..69: Result:= 'C POW';
    70..79: Result:= 'B POW';
    80..89: Result:= 'A POW';
    90..127: Result:= 'S POW';
  end;
end;

function TForm1.calcular(hProcess: THandle): string;
var
  temp: cardinal;
  MyLP: Word;
  Vitory, Turn, EffectATK,
  DefWin, DownFace, Fusoes, Combo,
  Equips, Magias, Traps, UseCards: Byte;
begin
  ReadProcessMemory(hProcess,ptr(GetEmule($B419A4)), Addr(MyLP),2,temp);
  ReadProcessMemory(hProcess,ptr(GetEmule($B41990)), Addr(Vitory),1,temp);
  ReadProcessMemory(hProcess,ptr(GetEmule($B41991)), Addr(Turn),1,temp);
  ReadProcessMemory(hProcess,ptr(GetEmule($B41992)), Addr(EffectATK),1,temp);
  ReadProcessMemory(hProcess,ptr(GetEmule($B41993)), Addr(DefWin),1,temp);
  ReadProcessMemory(hProcess,ptr(GetEmule($B41994)), Addr(DownFace),1,temp);
  ReadProcessMemory(hProcess,ptr(GetEmule($B41998)), Addr(Fusoes),1,temp);
  ReadProcessMemory(hProcess,ptr(GetEmule($B41999)), Addr(Equips),1,temp);
  ReadProcessMemory(hProcess,ptr(GetEmule($B41995)), Addr(Magias),1,temp);
  ReadProcessMemory(hProcess,ptr(GetEmule($B41996)), Addr(Traps),1,temp);
  ReadProcessMemory(hProcess,ptr(GetEmule($B41997)), Addr(Combo),1,temp);
  ReadProcessMemory(hProcess,ptr(GetEmule($B419A8)), Addr(UseCards),1,temp);
  if bGanhaSTEC then
   Result := 'S TEC' else
   Result := calcularResultado(MyLP,
   Vitory, Turn, EffectATK,
   DefWin, DownFace, Fusoes,
   Equips, Magias, Traps, Combo, UseCards);
end;

procedure TForm1.btnInfoClick(Sender: TObject);
begin
   if Form3.Showing then
     Form3.Close else
     Form3.Show;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
 bkListView.Clear;
 FreeAndNil(bkListView);
 FreeAndNil(dataset);
end;

function TForm1.Dropar(NPC: Byte; qntDuel: Word; Rank: string; var Card: Word): Boolean;
var
  i, index, sProb, RandProb: Integer;
  rProb: Real;
  lista: TStringList;
  bRank: Smallint;
begin
  Result := False;
  lista := TStringList.Create;
  lista.Clear;

  for i:= 0 to Length(DropList) do
  if DropList[i].NPC = NPC then
  begin
     case AnsiIndexText(Rank,['S TEC','A TEC','S POW','A POW']) of
         0,1: bRank := 0;
         2,3: bRank := 1;
     else
        bRank := -1;
     end;

    if DropList[i].TVit = bRank then
    if DropList[i].Duel <= qntDuel then
    begin
       lista.Add(
       IntToStr(DropList[i].ID)+';'+
       FloatToStr(DropList[i].Prob)
       );
    end;
  end;

  if lista.Count > 0 then
  begin
   Randomize;
   index := RandomRange(0,lista.Count-1);
   Card := StrToIntDef(copy(lista.Strings[index],0,AnsiPos(';',lista.Strings[index])-1),0);

   // TESTES
   {TIProcess.Enabled := False;
   ShowMessage('Carta: ' + copy(lista.Strings[index],0,AnsiPos(';',lista.Strings[index])-1));
   ShowMessage('Probabilidade: '+ copy(lista.Strings[index],AnsiPos(';',lista.Strings[index])+1,Length(lista.Strings[index])));
   ShowMessage('Duelista: '+IntToStr(NPC));
   ShowMessage('Rank: '+Rank);
   ShowMessage('Total: '+IntToStr(lista.Count)+' Range: '+ IntToStr(index));
   ShowMessage('Quantidade de Duelos: '+IntToStr(qntDuel));
   TIProcess.Enabled := True;  }

   if (Card >= 1) and (Card <= 722) then
   begin
    rProb := StrToFloatDef(copy(lista.Strings[index],AnsiPos(';',lista.Strings[index])+1,Length(lista.Strings[index])),-1);
    if rProb >= 0 then
    begin
      if (rProb >= 0) and (rProb <= 0.25) then
          sProb := 40 else
      if (rProb >= 0.26) and (rProb <= 0.50) then
          sProb := 35 else
      if (rProb >= 0.51) and (rProb <= 0.75) then
          sProb := 30 else
      if (rProb >= 0.76) and (rProb <= 1) then
          sProb := 25 else
      if (rProb >= 1.01) and (rProb <= 2) then
          sProb := 20 else
      if (rProb >= 2.01) and (rProb <= 3) then
          sProb := 15 else
      if (rProb >= 3.01) and (rProb <= 4) then
          sProb := 10 else
      if (rProb >= 4.01) then
          sProb := 5;

      Randomize;
      RandProb := RandomRange(1,sProb);
      //btDB.Caption := IntToStr(Trunc(sProb div 2))+'='+IntToStr(RandProb);
      if RandProb = Trunc(sProb div 2) then
         Result := True;
     end;    
    end;
   end;

  FreeAndNil(lista);
end;

function TForm1.Estrelas(Rank: String): Integer;
begin
   Result := 0;
   case AnsiIndexText(Rank,['S TEC','A TEC','S POW','A POW','B POW','B TEC','C POW','C TEC','D POW','D TEC']) of
     0: Result := 4995;
     1: Result := 2996;
     2: Result := 995;
     3: Result := 496;
     4,5: Result := 297;
     6,7: Result := 198;
     8,9: Result := 99;
   end;
end;

function TForm1.qntDuelos(NPC: Byte; hProcess: THandle; const Grava: Boolean = False; const QNT: Word = 0): Word;
var
 sm: String;
 i1, i2: Integer;
 temp: cardinal;
begin
 sm := IntToHex(((NPC-1)*4),4);
 i1 := GetEmule(AD_VICTORYS);
 i2 := StrToInt('$' + sm);
 if not Grava then
  ReadProcessMemory(hProcess,ptr(i1+i2),Addr(Result),2,temp) else
  WriteProcessMemory(hProcess,ptr(i1+i2),Addr(QNT),2,temp);
end;

procedure TForm1.alterarTipo(ID: Word; var lbTipo: TsLabel);
begin
 if ID <= 0 then
 lbTipo.Caption := '' else
 lbTipo.Caption := bkListView.FindCaption(0,IntToStr(ID),True,True,True).SubItems[1];
end;

procedure TForm1.lbReduzeClick(Sender: TObject);
var
  sizepnlDebug: Integer;
begin
 sizepnlDebug := 1;

 if (sPaneDeb.Visible) then
    sizepnlDebug := sPaneDeb.Width;

 if not Config.wMax then
 begin
   sStatusBar1.Visible := False;
   sPanelF.Visible := False;
   sPanelM.Visible := False;
   sPanelO.Visible := False;
   sPanelC.Align := alNone;
   sPanelC.Left := 0;
   Form1.AutoSize := False;
   Form1.Width := sPanelC.Width + sizepnlDebug + 5;
   Config.wMax := True;
 end else
 begin
   Form1.AutoSize := False;
   Form1.Width := 551;
   sPanelC.Left := 390;
   sStatusBar1.Visible := True;
   sPanelF.Visible := True;
   sPanelC.Align := alRight;
  if Config.IsReliquiasOn then
  begin
   sPanelM.Visible := True;
   sPanelO.Visible := True;
  end;
   Form1.AutoSize := True;
   Config.wMax := False;   
 end;
 sSkinManager1.RepaintForms;
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 Config.wPosX := Form1.Left;
 Config.wPosY := Form1.Top;
 salvarConfig(True);
 UnRegisterHotKey(Handle, HKeyMinimize);
 GlobalDeleteAtom(HKeyMinimize);
 UnRegisterHotKey(Handle, HKeyDB);
 GlobalDeleteAtom(HKeyDB);
 UnRegisterHotKey(Handle, HKEYGuia);
 GlobalDeleteAtom(HKEYGuia);
 RemoveFontMemResourceEx(hFontYuGiOh);
end;

procedure TForm1.TipoBonusReliquia(var Reliquia: fReliquia; IDs, IDFs: array of Word; card_Exist, card_ExistF: array of Byte; hProcess: THandle);
  function verificarNotEQUIP(X: Integer): Boolean;
  begin
   Result := True;
   if (X > 0) and (X < 32766) then
    Result := False;
  end;
var
  X,Y: Integer;
  smATK, smDEF, bufEQP: Word;
  bufTurn: Byte;
  s: string;
  temp: Cardinal;
begin
  s := '';
  ReadProcessMemory(hProcess,ptr(GetEmule(AD_TURN)), Addr(bufTurn),1,temp);
  case Reliquia.Tipo of
    0: begin // Ganha Bonus ATK/DEF com turno limite
         for x:= 0 to 4 do
         begin
          ReadProcessMemory(hProcess,ptr(GetEmule(AD_CARD_YOU[x+1])+6), Addr(bufEQP),2,temp);
          if (IDs[x] = Reliquia.ID) and ((x+1) = Reliquia.sATc) then
          if card_Exist[x] > 0 then
          begin
           smATK := 0;
           smDEF := 0;
           s := '0';

            if not Reliquia.sAT then
            begin
             Reliquia.sTurn := bufTurn;
             Reliquia.sTurnE := Reliquia.sTurn + Reliquia.TurnF;
             Reliquia.sAT := True;
            end;

            if (bufTurn < Reliquia.sTurnE) and (verificarNotEQUIP(bufEQP)) then
            begin
             smATK := Reliquia.ATK + Reliquia.ATKG;
             smDEF := Reliquia.DEF + Reliquia.DEFG;
             s := IntToStr((Reliquia.sTurnE - bufTurn));
            end else
            begin
             smATK := Reliquia.ATK;
             smDEF := Reliquia.DEF;
            end;

           case Reliquia.sATc of
             1: begin
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is1)+2), Addr(smATK),2,temp);
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is1)+4), Addr(smDEF),2,temp);
                end;
             2: begin
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is2)+2), Addr(smATK),2,temp);
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is2)+4), Addr(smDEF),2,temp);
                end;
             3: begin
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is3)+2), Addr(smATK),2,temp);
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is3)+4), Addr(smDEF),2,temp);
                end;
             4: begin
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is4)+2), Addr(smATK),2,temp);
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is4)+4), Addr(smDEF),2,temp);
                end;
             5: begin
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is5)+2), Addr(smATK),2,temp);
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is5)+4), Addr(smDEF),2,temp);
                end;
           end;
           TsLabel(FindComponent('lbSymbol'+IntToStr(Reliquia.sATc))).Caption := 'Turn: '+s;
          end;
         end;
       end;
    1: begin // Ganha Bonus por outra carta Sua.
         for x:= 0 to 4 do
         begin
          smATK := 0;
          smDEF := 0;
          s := '0';
          Reliquia.sATc := 0;

            for y:= 0 to 4 do
            if IDs[y] = Reliquia.Tp_Monst then
            if card_Exist[y] > 0 then
            begin
             smATK := smATK + Reliquia.ATKG;
             smDEF := smDEF + Reliquia.DEFG;
             s := IntToStr(StrToInt(s)+1);
            end;

          if IDs[x] = Reliquia.ID then
          begin
           smATK := smATK + Reliquia.ATK;
           smDEF := smDEF + Reliquia.DEF;
           case x of
             0: begin
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is1)+2), Addr(smATK),2,temp);
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is1)+4), Addr(smDEF),2,temp);
                end;
             1: begin
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is2)+2), Addr(smATK),2,temp);
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is2)+4), Addr(smDEF),2,temp);
                end;
             2: begin
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is3)+2), Addr(smATK),2,temp);
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is3)+4), Addr(smDEF),2,temp);
                end;
             3: begin
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is4)+2), Addr(smATK),2,temp);
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is4)+4), Addr(smDEF),2,temp);
                end;
             4: begin
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is5)+2), Addr(smATK),2,temp);
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is5)+4), Addr(smDEF),2,temp);
                end;
           end;
           TsLabel(FindComponent('lbSymbol'+IntToStr(x+1))).Caption := 'QT: '+s;
          end;
         end;
       end;
    2: begin // Ganha Bonus por carta inimiga.
        if Reliquia.sAT then
        begin

        end else
        begin

        end;
       end;
    3: begin // Ganha Bonus por tipo de Monstro Seu;
        for x:= 0 to 4 do
         begin
          smATK := 0;
          smDEF := 0;
          s := '0';
          Reliquia.sATc := 0;

          if Reliquia.ID > 0 then
          begin
            for y:= 0 to 4 do
            if IDs[y] > 0 then
            if bkListView.Items.Item[IDs[y]-1].SubItems[2] = Tipo[Reliquia.Tp_Monst-1] then
            if card_Exist[y] > 0 then
            begin
             smATK := smATK + Reliquia.ATKG;
             smDEF := smDEF + Reliquia.DEFG;
             s := IntToStr(StrToInt(s)+1);
            end;

            if (card_Exist[x] > 0) and
               (bkListView.Items.Item[IDs[x]-1].SubItems[2] = Tipo[Reliquia.Tp_Monst-1]) then
            begin
             smATK := smATK + Reliquia.ATK;
             smDEF := smDEF + Reliquia.DEF;
             TsLabel(FindComponent('lbSymbol'+IntToStr(x+1))).Caption := 'QT: '+s;
            end;
          end else
          if (card_Exist[x] > 0) then
          if bkListView.Items.Item[IDs[x]-1].SubItems[2] = Tipo[Reliquia.Tp_Monst-1] then
          begin
            smATK := StrToIntDef(bkListView.Items.Item[IDs[x]-1].SubItems[5],-1) + Reliquia.ATKG;
            smDEF := StrToIntDef(bkListView.Items.Item[IDs[x]-1].SubItems[6],-1) + Reliquia.DEFG;
            TsLabel(FindComponent('lbSymbol'+IntToStr(x+1))).Caption := 'POWER';
          end;
          
          if (card_Exist[x] > 0) then
          if bkListView.Items.Item[IDs[x]-1].SubItems[2] = Tipo[Reliquia.Tp_Monst-1] then
          case x of
             0: begin
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is1)+2), Addr(smATK),2,temp);
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is1)+4), Addr(smDEF),2,temp);
                end;
             1: begin
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is2)+2), Addr(smATK),2,temp);
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is2)+4), Addr(smDEF),2,temp);
                end;
             2: begin
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is3)+2), Addr(smATK),2,temp);
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is3)+4), Addr(smDEF),2,temp);
                end;
             3: begin
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is4)+2), Addr(smATK),2,temp);
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is4)+4), Addr(smDEF),2,temp);
                end;
             4: begin
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is5)+2), Addr(smATK),2,temp);
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is5)+4), Addr(smDEF),2,temp);
                end;
           end;
         end;
       end;
    4: begin // Ganha Bonus por tipo de Monstro Inimigo;
        for x:= 0 to 4 do
         begin
          smATK := 0;
          smDEF := 0;
          s := '0';
          Reliquia.sATc := 0;
          
            for y:= 0 to 4 do
            if IDFs[y] > 0 then
            if bkListView.Items.Item[IDFs[y]-1].SubItems[2] = Tipo[Reliquia.Tp_Monst-1] then
            if card_ExistF[y] > 0 then
            begin
             smATK := smATK + Reliquia.ATKG;
             smDEF := smDEF + Reliquia.DEFG;
             s := IntToStr(StrToInt(s)+1);
            end;

          if IDs[x] = Reliquia.ID then
          begin
           smATK := smATK + Reliquia.ATK;
           smDEF := smDEF + Reliquia.DEF;
           case x of
             0: begin
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is1)+2), Addr(smATK),2,temp);
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is1)+4), Addr(smDEF),2,temp);
                end;
             1: begin
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is2)+2), Addr(smATK),2,temp);
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is2)+4), Addr(smDEF),2,temp);
                end;
             2: begin
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is3)+2), Addr(smATK),2,temp);
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is3)+4), Addr(smDEF),2,temp);
                end;
             3: begin
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is4)+2), Addr(smATK),2,temp);
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is4)+4), Addr(smDEF),2,temp);
                end;
             4: begin
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is5)+2), Addr(smATK),2,temp);
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is5)+4), Addr(smDEF),2,temp);
                end;
           end;
           TsLabel(FindComponent('lbSymbol'+IntToStr(x+1))).Caption := 'QT: '+s;
          end;
         end;
       end;
    5: begin // Ganha Bonus por quantidade de LP Seu;
        if Reliquia.sAT then
        begin

        end else
        begin

        end;
       end;
    6: begin // Ganha Bonus por quantidade de LP Inimigo;
        if Reliquia.sAT then
        begin

        end else
        begin

        end;
       end;
    7: begin // Ganha Bonus por quantidade de cartas Monstros na mesa Sua;
        if Reliquia.sAT then
        begin

        end else
        begin

        end;
       end;
    8: begin // Ganha Bonus por quantidade de cartas Monstros na mesa Inimiga;
        if Reliquia.sAT then
        begin

        end else
        begin

        end;
       end;
    9: begin // Ganha Bonus por quantidade de cartas Magicos na mesa Sua;
        if Reliquia.sAT then
        begin

        end else
        begin

        end;
       end;
    10: begin // Ganha Bonus por quantidade de cartas Magicos na mesa Inimiga;
         if Reliquia.sAT then
         begin

         end else
         begin

         end;
        end;
    11: begin // Ganha Bonus aumentando ATK e DEF por Turn
         for x:= 0 to 4 do  // TODO
         begin
          if (IDs[x] = Reliquia.ID) and ((x+1) = Reliquia.sATc) then
          if card_Exist[x] > 0 then
          begin
           smATK := 0;
           smDEF := 0;
           s := '0';

            if not Reliquia.sAT then
            begin
             Reliquia.sTurn := bufTurn;
             Reliquia.sTurnE := Reliquia.sTurn + Reliquia.TurnF;
             Reliquia.sAT := True;
            end else
            if bufTurn < Reliquia.sTurnE then
            begin
             smATK := Reliquia.ATK + (Reliquia.ATKG * (Reliquia.TurnF -(Reliquia.sTurnE - bufTurn)));
             smDEF := Reliquia.DEF + (Reliquia.DEFG * (Reliquia.TurnF -(Reliquia.sTurnE - bufTurn)));
             s := IntToStr(Reliquia.TurnF -(Reliquia.sTurnE - bufTurn));
            end else
            begin
             smATK := Reliquia.ATK + (Reliquia.ATKG * Reliquia.TurnF);
             smDEF := Reliquia.DEF + (Reliquia.DEFG * Reliquia.TurnF);
             s := IntToStr(Reliquia.TurnF);
            end;

           case Reliquia.sATc of
             1: begin
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is1)+2), Addr(smATK),2,temp);
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is1)+4), Addr(smDEF),2,temp);
                end;
             2: begin
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is2)+2), Addr(smATK),2,temp);
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is2)+4), Addr(smDEF),2,temp);
                end;
             3: begin
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is3)+2), Addr(smATK),2,temp);
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is3)+4), Addr(smDEF),2,temp);
                end;
             4: begin
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is4)+2), Addr(smATK),2,temp);
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is4)+4), Addr(smDEF),2,temp);
                end;
             5: begin
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is5)+2), Addr(smATK),2,temp);
                 WriteProcessMemory(hProcess,ptr(GetEmule(Codes.Is5)+4), Addr(smDEF),2,temp);
                end;
           end;
           TsLabel(FindComponent('lbSymbol'+IntToStr(Reliquia.sATc))).Caption := 'Nv: '+s;
          end;
         end;
        end;
  end;
end;

function TForm1.StatusCard(isCard: Byte): Byte;
begin
   Result := 0;
   case isCard of
     1..4: Result := 1; // atk recebido ou substitui��o da carta ou em ataque.
     5..127: Result := 2; // atk recebido, carta destruida.
     128..135,160..167: Result := 05; // atk posi��o de frente
     136..143,168..175: Result := 10; // def posi��o de frente
     144..151,176..183: Result := 15; // atk posi��o de tr�s
     152..159,184..191: Result := 20; // def posi��o de tr�s
     192..199,224..231: Result := 105; // atk posi��o de frente inativo
     200..207,232..239: Result := 110; // def posi��o de frente inativo
     208..215,240..247: Result := 115; // atk posi��o de tr�s inativo
     216..223,248..255: Result := 120; // def posi��o de tr�s inativo
   end;
end;

function TForm1.IsSubstituirCard(isCard: Byte;
  isCardF: array of Byte): Boolean;
var
  i: Byte;
begin
  Result := False;
  if (isCard = 4) or (isCard = 12) or      // 4 = atk, 12 = def, 20 = atk de tras
     (isCard = 20) or (isCard = 28) then
  begin
    for i:= 0 to Length(isCardF)-1 do
    if (isCardF[i] > 0) and (isCardF[i] < 128) then
    begin
      Break;
      Exit;
    end;
  end else
      Exit;

  Result := True;
end;

function TForm1.StatusCardIsATKDEF(IsCard: Byte): Byte;
begin
  case StatusCard(isCard) of
   0: Result := 0;
   5,15,105,115: Result := 1;
   10,20,110,120: Result := 2;
   else
    Result := 255;
  end;
end;

procedure TForm1.sCheckEmuClick(Sender: TObject);
begin
 cbEmulador.ReadOnly := not sCheckEmu.Checked;
 if sCheckEmu.Checked then
  cbEmulador.Style := csDropDownList else
  cbEmulador.Style := csDropDown;
 btRun.Enabled := sCheckEmu.Checked;
end;

procedure TForm1.cbProcessosChange(Sender: TObject);
begin
  verificarEmulador;
end;

procedure TForm1.verificarEmulador;  // problema...
const
  EmuladorDescOK = 'Emulador Compat�vel! ';
  EmuladorDescFail = 'FALHOU! Processo selecionado inv�lido ou n�o compat�vel!';
var
  Process: THandle;
  compara: Boolean;
  EmuNumber: array[1..10] of Byte;
  x, y: Integer;
  temp: cardinal;
  sPID: string;
begin
 isGoPID := cbProcessos.Items.Strings[cbProcessos.ItemIndex];
 sPID := Copy(cbProcessos.Items.Strings[cbProcessos.ItemIndex],
                 AnsiPos(' [',cbProcessos.Items.Strings[cbProcessos.ItemIndex])+2,
                 Length(cbProcessos.Items.Strings[cbProcessos.ItemIndex]));
 sPID := Copy(sPID,0,Length(sPID)-1);

 try
  PID := StrToInt(sPID);
 except
  Application.MessageBox('Ocorreu algum erro na inje��o.','Valor de PID inv�lido!',0);
  Exit;
 end;

  Process := OpenProcess(PROCESS_ALL_ACCESS,FALSE,PID);
  for x := 0 to Length(Emuladores[0].Code)-1 do
  begin
   ReadProcessMemory(Process,ptr(StrToInt('$' + AD_EPSXE)+x), Addr(EmuNumber[x]),1,temp);
  end;

  for x := 0 to Length(Emuladores)-1 do
  begin
   if (SizeOf(Emuladores[x].Code) = SizeOf(EmuNumber)) then
   for y:= 0 to Length(Emuladores[x].Code)-1 do
   if Emuladores[x].Code[y] = EmuNumber[y] then
    compara := True else
   begin
    compara := False;
    Break;
   end;

   if compara then
   begin
      cbEmulador.ItemIndex := x;
      sLabelRunEmu.Caption := EmuladorDescOK + cbEmulador.Text;
      sLabelRunEmu.Font.Color := clGreen;
      sCheckEmu.Checked := False;
      btRun.Enabled := True;
      break;
   end;
  end;

  if not compara then
  begin
    cbEmulador.ItemIndex := -1;
    cbEmulador.Text := 'Processo Desconhecido! Selecione Manualmente...';
    sLabelRunEmu.Caption := EmuladorDescFail;
    sLabelRunEmu.Font.Color := clRed;
  end;

  try
   CloseHandle(Process);
  except
    end;
end;

procedure TForm1.cbProcessosClick(Sender: TObject);
begin
  verificarEmulador;
end;

function TForm1.GetEmule(Endereco: Variant): Integer; // or Word;
var
  isPlus: Boolean;
  valor, fEndereco: Integer;
begin
 Result := 0;
 IsPlus := False;
 if Emuladores[Config.wEmu].SunNumber[1] = 'P' then
    isPlus := True;
 valor := StrToIntDef(Copy(Emuladores[Config.wEmu].SunNumber,2,20),0);

 case VarType(Endereco) of
  varString: begin
              fEndereco := StrToInt('$' + VarToStr(Endereco));
             end;
  varSmallint,  varByte,  varWord,  varInteger,  varInt64,  varShortInt,
  varLongWord: begin
                fEndereco := Endereco;
               end;
 else
  if TIProcess.Enabled then
   btRun.Click;
  ShowMessage('Tipo de Dado do Valor de Endere�o Inv�lido: ' + Endereco);
  Exit;
 end;

 if isPlus then
  Result := fEndereco + valor else
  Result := fEndereco - valor;
end;
 
procedure TForm1.sWebLabel5Click(Sender: TObject);
var
  i: Integer;
  s: string;
begin
  s := 'Lista com todos os emuladores compat�veis:'+#13;
  for i:= 0 to Length(Emuladores)-1 do
  s := s + IntToStr(i+1) + '. ' + Emuladores[i].Nome + #13;
  s := s + #13 + 'Vers�o Recomendada: Epsxe 1.9.0';
  ShowMessage(s);
end;

procedure TForm1.sWebLabel3Click(Sender: TObject);
begin
 ShowMessage('Selecionar Manualmente significa que mesmo voc� selecionando o processo do emulador, o m�dulo n�o consegue reconhec�-lo como compat�vel!'+
              #13+'Mas a possibilidade de funcionar a inje��o manual � bem remota!');
end;

procedure TForm1.notMegaMorph(hProcess: THandle);
var
  isValue: Byte;
  temp: cardinal;
begin
 ReadProcessMemory(hProcess,ptr(GetEmule(AD_PASS_MEGA)), Addr(IsValue),1,temp);
 case IsValue of
   0..63: IsValue := IsValue + 64;
   128..191: IsValue := IsValue + 64;
 end;
 WriteProcessMemory(hProcess,ptr(GetEmule(AD_PASS_MEGA)), Addr(IsValue),1,temp);
end;

procedure TForm1.lbDescBiomeClick(Sender: TObject);
begin
  if Config.IsDebug77 then
  begin
    sPaneDeb.Visible := not sPaneDeb.Visible;
    if (sPaneDeb.Visible) then
    begin
      pnlDebug.Show;
      Form1.Width :=  sPanelBG.Width + pnlDebug.Width;
      Form1.Left := Form1.Left - pnlDebug.Width;
      sPanelBG.Left := pnlDebug.Width;
    end else
    begin
      Form1.Width :=  sPanelBG.Width - pnlDebug.Width;
      Form1.Left := Form1.Left + pnlDebug.Width;
      sPanelBG.Left := 0;
      pnlDebug.Hide;
    end;
    sPaneDeb.BringToFront;
    sSkinManager1.RepaintForms;
  end;
end;

procedure TForm1.sSpeedButtonOkEditClick(Sender: TObject);
var
  i: Integer;
  hProcess: THandle;
  bufLP: Word;
  IsGeneric: Byte;
  temp: Cardinal;
begin
  for i:= 0 to sPaneDeb.ControlCount-1 do
  if sPaneDeb.Controls[i] is TsCheckBox then
  begin
     (sPaneDeb.Controls[i] as TsCheckBox).Enabled := True;
     (sPaneDeb.Controls[i] as TsCheckBox).Font.Style := [];
  end;
  sSpinEditLPorDeck.Enabled := False;
  sSpeedButtonOkEdit.Enabled := False;

  //escreve memoria.
  if TIProcess.Enabled then
  begin
    if Config.IsDebug77 then
    begin
     hProcess := OpenProcess(PROCESS_ALL_ACCESS,FALSE,PID);
     if sSpinEditLPorDeck.Tag = 3 then
     begin
      IsGeneric := 40 - sSpinEditLPorDeck.Value;
      WriteProcessMemory(hProcess,ptr(GetEmule($B419A8)), Addr(IsGeneric),1,temp);
     end;
     if sSpinEditLPorDeck.Tag = 4 then
     begin
      IsGeneric := 40 - sSpinEditLPorDeck.Value;
      WriteProcessMemory(hProcess,ptr(GetEmule($B419C8)), Addr(IsGeneric),1,temp);
     end;
     if sSpinEditLPorDeck.Tag = 2 then
     begin
      bufLP := sSpinEditLPorDeck.Value;
      WriteProcessMemory(hProcess,ptr(GetEmule($B419C4)), Addr(bufLP),2,temp);
     end;
     if sSpinEditLPorDeck.Tag = 1 then
     begin
      bufLP := sSpinEditLPorDeck.Value;
      WriteProcessMemory(hProcess,ptr(GetEmule($B419A4)), Addr(bufLP),2,temp);
     end;
    end;
  end else
  ShowMessage('O M�dulo n�o est� ativo!');
end;

procedure TForm1.sCheckLPYouMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  i: Integer;
begin
 if X < 15 then
 Exit else
  TsCheckBox(Sender).Checked := not  TsCheckBox(Sender).Checked;
 if TsCheckBox(Sender).Tag in[1,2] then
 begin
  sSpinEditLPorDeck.MaxValue := 9999;
  sSpinEditLPorDeck.MaxLength := 4;
 end else
 begin
  sSpinEditLPorDeck.MaxValue := 99;
  sSpinEditLPorDeck.MaxLength := 2;
 end;

  sSpinEditLPorDeck.Value := StrToInt(TsCheckBox(Sender).Caption);
  sSpinEditLPorDeck.Tag := TsCheckBox(Sender).Tag;
  TsCheckBox(Sender).Font.Style := [fsUnderline];

  for i:= 0 to sPaneDeb.ControlCount-1 do
  if sPaneDeb.Controls[i] is TsCheckBox then
  (sPaneDeb.Controls[i]).Enabled := False;

  sSpinEditLPorDeck.Enabled := True;
  sSpeedButtonOkEdit.Enabled := True;
  sSpinEditLPorDeck.SetFocus;
end;

procedure TForm1.sSpinEditLPorDeckExit(Sender: TObject);
begin
 if sSpinEditLPorDeck.Value < 0 then
    sSpinEditLPorDeck.Value := 0;

 if sSpinEditLPorDeck.Tag in[1,2] then
 begin
  if sSpinEditLPorDeck.Value > 9999 then
     sSpinEditLPorDeck.Value := 9999;
 end else
  if sSpinEditLPorDeck.Value > 40 then
     sSpinEditLPorDeck.Value := 40;
end;

procedure TForm1.slbNotVisiblePaneDEBClick(Sender: TObject);
begin
 sPaneDeb.Visible := False;
end;

procedure TForm1.sSpinEditLPorDeckKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if key = VK_RETURN then
    sSpeedButtonOkEdit.Click;
end;

procedure TForm1.btnDebugInfoClick(Sender: TObject);
var
  i: Integer;
  s: string;
begin
  if Config.IsDebug77 then
  begin
    s := '';
    for i:=0 to Length(Enigma)-1 do
    s := s+#13+'ID: '+IntToStr(Enigma[i].ID)+'; AT: '+
           BoolToStr(Enigma[i].sAT)+'; ATc: '+
           IntToStr(Enigma[i].sATc)+'; Turn: '+
           IntToStr(Enigma[i].sTurn)+'; TurnoFim: '+
           IntToStr(Enigma[i].sTurnE);
    ShowMessage(s);
  end;
end;

procedure TForm1.btnDebugAllCardsClick(Sender: TObject);
  function aplicar(PID: THandle; Index: Integer; msg: string): Boolean;
  var
     i: Integer;
     isValue, isDuel: Byte;
     IsAddress: Integer;
     isStar, temp: Cardinal;
  begin
    if MessageDlg(msg, mtConfirmation,[mbYes,mbNo], 0) = mrYes then
    begin
      case Index of
        1: begin //Todas as cartas
             if sStatusBar1.Panels.Items[0].Text = 'YuGiOh My Deck' then
             begin
                isValue := 10;
                for i:= 0 to 1443 do
                begin
                 IsAddress := GetEmule('B5D738') + StrToInt('$' + IntToHex(i,1));
                 WriteProcessMemory(PID,ptr(IsAddress),Addr(isValue),1,temp);
                end;
                ShowMessage('Ok, agora voc� possui todos as Cartas! Saia do BUILD DECK e volte novamente!');
             end else ShowMessage('Para Obter todas as Cartas, voc� dever� estar na BUILD DECK!');
           end;
        2: begin //Todas as estrelas
              isStar := 999999;
              WriteProcessMemory(PID,ptr(GetEmule($C28180)), Addr(isStar),4,temp);
              ShowMessage('Ok, agora voc� possui 999999 Estrelas! Se voc� estiver no PASSWORD, provavelmente dever�s atualizar sua tela saindo desta e voltando...');
           end;
        3: begin // Todos os Duelistas
              isValue := 255;
              for i:= 0 to 5 do
              begin
               IsAddress := GetEmule('C28094') + StrToInt('$' + IntToHex(i,1));
               WriteProcessMemory(PID,ptr(IsAddress),Addr(isValue),1,temp);
              end;
              ShowMessage('Ok, agora voc� possui todos os duelistas! Se voc� estiver no FREE DUEL, provavelmente dever�s atualizar sua tela saindo desta e voltando...');
           end;
        4: begin // Sair da partida atual com vit�ria e sem carta recompensa.
             ReadProcessMemory(PID,ptr(GetEmule(AD_IsDuel)), Addr(isDuel),1,temp);
             if isDuel = 1 then
             begin
              isValue := 255;
              WriteProcessMemory(PID,ptr(GetEmule($AF2B0D)), Addr(isValue),1,temp);
             end else
             ShowMessage('Voc� deve est� dentro de uma partida para poder sair dela!');
           end;
      end;
      Result:= True;
    end else Result:= False;
  end;
const
  MSGs: array[1..4] of String = ('Deseja obter todas as cartas do jogo?',
                                 'Deseja obter o m�ximo de estrelas?',
                                 'Deseja obter todos os duelistas?',
                                 'Sair realmente da partida?'#13#13'Observa��o: N�o haver� recompensa de carta ao sair.');
var
 hProcess: THandle;
 index: Integer;
begin
  index := 0;
  if (Sender is TSpeedButton) then
  begin
    index := (Sender as TSpeedButton).Tag;
  end;
  if (Config.IsDebug77) and (TIProcess.Enabled) then
  begin
    hProcess := OpenProcess(PROCESS_ALL_ACCESS,FALSE,PID);
    aplicar(hProcess, index, MSGs[index]);
    CloseHandle(hProcess);
  end else
    ShowMessage('O M�dulo n�o est� ativo!');
end;

procedure TForm1.btnDebugDuelsModClick(Sender: TObject);
function tipoRank(i: Byte): string;
 begin
   if i = 1 then
    Result := 'SA POW' else Result := 'SA TEC';
 end;
 function espacoCerto(s: String): string;
 begin
   if Length(s) < 8 then
    Result := s+#9+#9 else
   if Length(s) < 16 then
    Result := s+#9 else
    Result := s+' ';
 end;
var
  i: Integer;
  s: string;
  hProcess: THandle;
begin
  if Config.IsDebug77 then
  begin
    s := 'ID	NPC'+#9+#9+#9+'Duelos	Rank';
    for i:=0 to Length(DropList)-1 do
    if DropList[i].Duel > 0 then
    begin
     s := s +#13+ IntToStr(DropList[i].ID)+#9+
                  espacoCerto(NPC[DropList[i].NPC])+#9+
                  IntToStr(DropList[i].Duel)+#9+
                  tipoRank(DropList[i].TVit);
    end;
    if MessageDlg(s+#13+'Deseja obter os pontos de vit�ria necess�rio para dropar cartas custom?',mtConfirmation,[mbYes,mbNo],0) = mrYes then
    begin
     if TIProcess.Enabled then
     begin
       hProcess := OpenProcess(PROCESS_ALL_ACCESS,FALSE,PID);
       for i:=0 to Length(DropList)-1 do
       if DropList[i].Duel > 0 then
       if qntDuelos(DropList[i].NPC,hProcess) <= DropList[i].Duel then
          qntDuelos(DropList[i].NPC,hProcess,True,DropList[i].Duel+1);
       ShowMessage('Pontos de vit�ria incluso com sucesso!');
     end else
      ShowMessage('O M�dulo n�o est� ativo!');
    end;
  end;
end;

procedure TForm1.lbDescBiomeMouseEnter(Sender: TObject);
begin
 if Config.IsDebug77 then
 begin
  lbDescBiome.Font.Color := clNavy;
 end;
end;

procedure TForm1.lbDescBiomeMouseLeave(Sender: TObject);
begin
 if Config.IsDebug77 then
 begin
  if (Config.wSkin = 2) then
    lbDescBiome.Font.Color := clWhite
  else lbDescBiome.Font.Color := clBlack;
 end;
end;

procedure TForm1.imgNPCClick(Sender: TObject);
begin
  if (imgNPC.Tag > 0) then
  begin
    if not FormDB.Showing then
      btDB.Click;
    FormDB.Procurar(1, IntToStr(imgNPC.Tag), '');
    if (FormDB.ListView1.Items.Count > 0) then
    begin
        FormDB.ListView1.ItemIndex := 0;
        FormDB.ListView1.OnClick(FormDB);
    end;
  end;
end;

end.
