program Project1;

uses
  Forms,
  Windows,
  Unit1 in 'Unit1.pas' {Form1},
  Unit2 in 'Unit2.pas' {Form2},
  YGODB in 'YGODB.pas' {FormDB},
  Unit3 in 'Unit3.pas' {Form3};

{$R *.res}

var
  ExtendedStyle : Integer;
  //PHandle: THandle;

begin
  Application.Initialize;
  ExtendedStyle := GetWindowLong(Application.Handle, GWL_EXSTYLE);
  SetWindowLong(Application.Handle, GWL_EXSTYLE, ExtendedStyle or
    WS_EX_PALETTEWINDOW and not WS_EX_TOPMOST);

  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TFormDB, FormDB);
  Application.CreateForm(TForm2, Form2);
  Application.CreateForm(TForm3, Form3);
  Application.Run;
end.
