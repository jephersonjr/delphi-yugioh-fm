unit YGODB;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, Math, ExtCtrls, XPMan, CheckLst, Buttons,
  jpeg, ImgList, acPNG, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinsDefaultPainters,
  sSkinProvider, sSkinManager, cxListBox, acProgressBar, sButton,
  sGroupBox, sEdit, sSpeedButton, sLabel, sPanel, cxContainer, cxEdit,
  cxListView, sCheckBox;

type
  TFormDB = class(TForm)
    PanelFilters: TsPanel;
    RadioGroup1: TsRadioGroup;
    Edit1: TsEdit;
    RadioGroup2: TsRadioGroup;
    ListView2: TcxListView;
    SpeedButton1: TsSpeedButton;
    SpeedButton2: TsSpeedButton;
    SpeedButton4: TsSpeedButton;
    SpeedButton5: TsSpeedButton;
    Edit2: TsEdit;
    Label1: TsLabel;
    Label2: TsLabel;
    PanelDetails: TsPanel;
    pnlImg: TsPanel;
    imgShow: TImage;
    lbNome: TsLabel;
    lbTipoCard: TsLabel;
    lbTipo: TsLabel;
    lbATK: TsLabel;
    lbDEF: TsLabel;
    lbSymbol: TsLabel;
    lbDescSymbol: TsLabel;
    lbEstrelas: TsLabel;
    lblDescPassCusto: TsLabel;
    lbPass: TsLabel;
    grp1: TsGroupBox;
    lbl1: TsLabel;
    lv1: TcxListView;
    lv2: TcxListView;
    lbl2: TsLabel;
    lbl3: TsLabel;
    lv3: TcxListView;
    dlgSave1: TSaveDialog;
    lblNum: TsLabel;
    dlgOpen1: TOpenDialog;
    ilImgNPC: TImageList;
    imgBKP: TImage;
    imgType: TImage;
    ilImgType: TImageList;
    ilImgCType: TImageList;
    sBtnSearch: TsSpeedButton;
    sSpeedButtonPass: TsSpeedButton;
    lbCusto: TsLabel;
    sLabelVits: TsLabel;
    sCheckShowIMG: TsCheckBox;
    PanelList: TsPanel;
    btnExport: TsButton;
    btnExportDrop: TsButton;
    btnMaxMin: TsSpeedButton;
    ListView1: TcxListView;
    panelBG: TsPanel;
    procedure FormCreate(Sender: TObject);
    procedure ListView1ColumnClick(Sender: TObject; Column: TListColumn);
    procedure ListView1Compare(Sender: TObject; Item1, Item2: TListItem;
      Data: Integer; var Compare: Integer);
    procedure RadioGroup2Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure ListView2Click(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
    procedure btnExportClick(Sender: TObject);
    procedure ListView1Click(Sender: TObject);
    procedure btnExportDropClick(Sender: TObject);
    procedure ListView1CustomDrawItem(Sender: TCustomListView;
      Item: TListItem; State: TCustomDrawState; var DefaultDraw: Boolean);
    procedure ListView1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edit1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure lv1CustomDrawItem(Sender: TCustomListView; Item: TListItem;
      State: TCustomDrawState; var DefaultDraw: Boolean);
    procedure sCheckShowIMGClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure sBtnSearchClick(Sender: TObject);
    procedure btnMaxMinClick(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sSpeedButtonPassClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Procurar(Index: Integer; S1,S2: String; const ignoreFilter: Boolean = False);
  end;

const
 star = '*';
 // SOL 1 / LUA 2 / Dark 3 / AntiDark 4
 // FOGO 5 / FOLHA 6 / AR 7 / TERRA 8 / ELETRO 9 / AGUA 10
 Symbol = ' s@gTVoY?iS';
 T_Card: array[0..5] of string = ('Equip','Field','Magic','Monster','Ritual','Trap');
 Tipo: array[0..20] of string = ('Aqua','Beast','Beast-Warrior','Dinosaur','Dragon',
 'Fairy','Fiend','Fish','Insect','Machine','Plant','Pyro','Reptile','Rock','Sea Serpent',
 'Spellcaster','Thunder','Warrior','Winged Beast','Zombie','');
 Simbolo: array[0..10] of string = ('','Sun','Moon','Mercury','Venus','Mars','Jupiter','Saturn',
 'Uranus','Pluto','Neptune');
 NPC: array[0..39] of string = ('','Simon Muran','Teana 1','Jono 1','Villager 1','Villager 2','Villager 3',
      'Seto 1','Heishin 1','Rex Raptor','Weevil Underwood','Mail Valentine','Bandit Keith','Shadi','Yami Bakura',
      'Pegasus','Isis','Kaiba','Mage Soldier','Jono 2','Teana 2','Ocean Mage','High Mage Secmenton',
      'Forest Mage','High Mage Anubisius','Mountain Mage','High Mage Atenza','Desert Mage','High Mage Martis',
      'Meadow Mage','High Mage Kepura','Labrynth Mage','Seto 2','Guardian Sebek','Guardian Neku','Heishin 2','Seto 3',
      'Dark Nite','Nitemare','Duel Master');
// Rank_BCD_19 = 'Simon Muran (0,29%)/Teana 1 (0,29%)/Villager 2 (0,59%)/Seto 1 (0,24%)/Heishin 1 (0,29%)/Rex Raptor (0,29%)/Weevil Underwood (0,29%)/Bandit Keith (0,59%)/'+
//               'Pegasus (0,59%)/Kaiba (0,59%)/Teana 2 (0,39%)/Forest Mage (0,29%)/High Mage Anubisius (0,29%)/Desert Mage (0,29%)/Meadow Mage (0,59%)/Meadow Mage (0,59%)/'+
//               'High Mage Kepura (0,29%)/Labrynth Mage (0,29%)/Guardian Sebek (0,29%)/Heishin 2 (0,29%)/Dark Nite (0,29%)/';
// Rank_BCD_20 = 'Jono 1 (0,29%)/Villager 1 (0,29%)/Villager 3 (0,39%)/Mail Valentine (0,49%)/Shadi (0,39%)/Yami Bakura (0,29%)/Isis (0,49%)/Mage Soldier (0,39%)/Jono 2 (0,29%)/'+
//               'Ocean Mage (0,29%)/High Mage Secmenton (0,34%)/Mountain Mage (0,29%)/High Mage Atenza (0,24%)/High Mage Martis (0,29%)/Guardian Neku (0,29%)/Seto 3 (0,29%)/Nitemare (0,29%)/Duel Master (0,39%)/';

var
  FormDB: TFormDB;
  Descending: Boolean;
  SortedColumn: Integer;

implementation

uses StrUtils, Unit1;

{$R *.dfm}

procedure separador(Texto: String; var lista: TStringList);
begin
 Texto := Trim(Texto);
 if Copy(Texto,Length(Texto),Length(Texto)) <> ';' then
 Texto := Texto + ';';
 while AnsiPos(';',Texto) > 0 do
 begin
  lista.Add(Copy(Texto,0,AnsiPos(';',Texto)-1));
  Delete(Texto,1,AnsiPos(';',Texto));
 end;
end;

procedure TFormDB.FormCreate(Sender: TObject);
begin
 RadioGroup2.OnClick(Self);
 btnExport.Caption := 'E'+#13+'X'+#13+'P'+#13+'O'+#13+'R'+#13+'T';
end;

procedure TFormDB.ListView1ColumnClick(Sender: TObject;
  Column: TListColumn);
begin
 TListView(Sender).SortType := stData;
 if Column.Index <> SortedColumn then
 begin
  SortedColumn := Column.Index;
  Descending := False;
 end
 else
  Descending := not Descending;
  TListView(Sender).SortType := stText;
end;

procedure TFormDB.ListView1Compare(Sender: TObject; Item1, Item2: TListItem;
  Data: Integer; var Compare: Integer);
begin
   if SortedColumn = 0 then
    Compare := CompareValue(StrToIntDef(Item1.Caption,-1), StrToIntDef(Item2.Caption,-1))
   else
   if ListView1.Columns.Count >= SortedColumn then
   if SortedColumn in[5..9] then
    Compare := CompareValue(StrToIntDef(Item1.SubItems[SortedColumn-1],-1),
                            StrToIntDef(Item2.SubItems[SortedColumn-1],-1)) else
    Compare := CompareText(Item1.SubItems[SortedColumn-1],
                           Item2.SubItems[SortedColumn-1]);
   if Descending then
    Compare := -Compare;
end;

procedure TFormDB.RadioGroup2Click(Sender: TObject);
var
 i: Integer;
begin
 if (RadioGroup2.ItemIndex >= 0) and
    (RadioGroup2.ItemIndex < RadioGroup2.Items.Count) then
 begin
  try
    ListView2.Items.BeginUpdate;
    case RadioGroup2.ItemIndex of
      0: begin
          ListView2.Clear;
          for i:= 0 to Length(T_Card)-1 do
          begin
            ListView2.Items.Add.Caption := T_Card[i];
            ListView2.Items.Item[ListView2.Items.Count-1].Checked := True;
          end;
         end;
      1: begin
          ListView2.Clear;
          for i:= 0 to Length(Tipo)-1 do
          begin
            ListView2.Items.Add.Caption := Tipo[i];
            ListView2.Items.Item[ListView2.Items.Count-1].Checked := True;
          end;
         end;
      2,3: begin
            ListView2.Clear;
            for i:= 0 to Length(Simbolo)-1 do
            begin
              ListView2.Items.Add.Caption := Simbolo[i];
              ListView2.Items.Item[ListView2.Items.Count-1].Checked := True;
            end;
           end;
      4..6: begin
            ListView2.Clear;
            for i:= 1 to Length(NPC)-1 do
            begin
              ListView2.Items.Add.Caption := NPC[i];
              ListView2.Items.Item[ListView2.Items.Count-1].Checked := True;
            end;
           end;
    end;
  finally
   ListView2.Items.EndUpdate;
  end;
 end;
end;

procedure SelecionarTodos(const Todos: Boolean = True; const Inverter: Boolean = False);
var
 i: Integer;
begin
 if FormDB.ListView2.Items.Count > 0 then
 for I:= 0 to FormDB.ListView2.Items.Count-1 do
 begin
  if Inverter then
  FormDB.ListView2.Items.Item[i].Checked := not FormDB.ListView2.Items.Item[i].Checked
  else
  FormDB.ListView2.Items.Item[i].Checked := Todos;
 end;
end;

procedure TFormDB.SpeedButton1Click(Sender: TObject);
begin
 SelecionarTodos();
end;

procedure TFormDB.SpeedButton2Click(Sender: TObject);
begin
 SelecionarTodos(False);
end;

procedure TFormDB.SpeedButton4Click(Sender: TObject);
begin
 SelecionarTodos(True,True);
end;

procedure TFormDB.SpeedButton5Click(Sender: TObject);
begin
 Edit1.Clear;
 Edit2.Clear;
end;

procedure TFormDB.ListView2Click(Sender: TObject);
begin
 ListView2.Items.BeginUpdate;
 if (ListView2.ItemIndex >= 0) and
    (ListView2.ItemIndex < ListView2.Items.Count) then
 begin
   ListView2.Items.Item[ListView2.ItemIndex].Checked := not
   ListView2.Items.Item[ListView2.ItemIndex].Checked;
 end;
 ListView2.Items.EndUpdate;
end;

// edit 1 tamanho width: 109 ou 269;

procedure TFormDB.RadioGroup1Click(Sender: TObject);
begin
 case RadioGroup1.ItemIndex of
   0: begin
       Edit1.Width := 269;
       Edit2.Clear;
       sBtnSearch.Click;
      end;
   1,6,7,8,10: begin
                Edit1.Width := 109;
               end;
 else begin
       Edit1.Width := 269;
       Edit2.Clear;
      end;
 end;
end;

function FiltroNaLista(filtro: string; var lista: TStringList): Boolean;
begin
 Result := False;
  case FormDB.RadioGroup2.ItemIndex of
    0: begin
        if lista.Count > 2 then
        if lista.Strings[2] = filtro then
           Result := True;
       end;
    1: begin
        if lista.Count > 3 then
        if lista.Strings[3] = filtro then
           Result := True;
       end;
    2: begin
        if lista.Count > 4 then
        if lista.Strings[4] = filtro then
           Result := True;
       end;
    3: begin
        if lista.Count > 10 then
        if lista.Strings[10] = filtro then
           Result := True;
       end;
    4: begin
        if lista.Count > 11 then
        if AnsiPos(filtro,lista.Strings[11]) > 0 then
           Result := True;
       end;
    5: begin
        if lista.Count > 12 then
        if AnsiPos(filtro,lista.Strings[12]) > 0 then
           Result := True;
       end;
    6: begin
        if lista.Count > 13 then
        if AnsiPos(filtro,lista.Strings[13]) > 0 then
           Result := True;
       end;
end;
end;

function FiltroDePesquisa(Index: Integer; s1,s2: string; var lista: TStringList): Boolean;
var
  isText: Boolean;
  i: Integer;
begin
 Result := False;
 isText := False;

 if Index = 0 then
 begin
   if s1 = '' then
    Result := True else
   for I:= 0 to lista.Count-1 do
   if AnsiPos(LowerCase(s1),LowerCase(lista.Strings[i])) > 0 then
   begin
    Result := True;
    Break;
   end;
    Exit;
 end else
 if Index = 12 then
 begin
   if AnsiPos('@',lista.Text) > 0 then
   if s1 = '' then
    Result := True else
   for I:= 0 to lista.Count-1 do
   if AnsiPos(LowerCase(s1),LowerCase(lista.Strings[i])) > 0 then
   begin
    Result := True;
    Break;
   end;
    Exit;
 end else
 if Index in[2,3,4,5,9,11] then
    isText := True;
    
 if s2 = '' then
    s2 := s1;   

 if isText then
 begin
   if lista.Count > Index then
   if AnsiPos(LowerCase(s1),LowerCase(lista.Strings[Index-1])) > 0 then
      Result := True;
 end else
 begin
   if lista.Count > Index then
   if StrToIntDef(lista.Strings[Index-1], -999999999) <> -999999999 then
   if (StrToInt(lista.Strings[Index-1]) >= StrToInt(s1)) and
      (StrToInt(lista.Strings[Index-1]) <= StrToInt(s2)) then
       Result := True;
 end;
end;

procedure TFormDB.Procurar(Index: Integer; S1,S2: String; const ignoreFilter: Boolean = False);
var
  i, y, w, x, size, sizeW: Integer;
  lista : TStringList;
  filtro: array of String;
  ok: Boolean;
begin
 FormDB.ListView1.Items.BeginUpdate;
 FormDB.ListView1.Clear;
 Descending := not Descending;
 FormDB.ListView1.OnColumnClick(FormDB.ListView1,FormDB.ListView1.Column[0]);
 lista := TStringList.Create;
 if (ignoreFilter) then
  size := 0 else
  size := FormDB.ListView2.Items.Count-1;
 sizeW := 1;

 for i:= 0 to size do
 if FormDB.ListView2.Items.Item[i].Checked then
 begin
  SetLength(Filtro,sizeW);
  sizeW := sizeW + 1;
  Filtro[sizeW - 2] := FormDB.ListView2.Items.Item[i].Caption;
 end;

 ok := False;

 for i := 0 to dataset.Count-1 do
 begin
  separador(dataset.Strings[i],lista);
  x := lista.Count-1;

  for y := 0 to Length(Filtro)-1 do
  if FiltroNaLista(trim(filtro[y]),lista) then
  begin
   ok := True;
   Break;
  end;

  if (ok) and (FiltroDePesquisa(Index,S1,S2,lista)) then
  begin
   for w := 0 to lista.Count-1 do
     if w = 0 then
       FormDB.ListView1.Items.Add.Caption := lista.Strings[w] else
     begin
       FormDB.ListView1.Items.Item[FormDB.ListView1.Items.Count-1].SubItems.Add(lista.Strings[w]);
     end;

   if FormDB.ListView1.Items.Count > 0 then
   if x < FormDB.ListView1.Columns.Count-1  then
   for w := 1 to (FormDB.ListView1.Columns.Count-1)-x do
     FormDB.ListView1.Items.Item[FormDB.ListView1.Items.Count-1].SubItems.Add('');

//   if FormDB.ListView1.Items.Item[FormDB.ListView1.Items.Count-1].Caption = '19' then
//     FormDB.ListView1.Items.Item[FormDB.ListView1.Items.Count-1].SubItems[12] := Rank_BCD_19 else
//   if FormDB.ListView1.Items.Item[FormDB.ListView1.Items.Count-1].Caption = '20' then
//     FormDB.ListView1.Items.Item[FormDB.ListView1.Items.Count-1].SubItems[12] := Rank_BCD_20;

   if FormDB.ListView1.Columns.Count > 8 then
   if FormDB.ListView1.Items.Item[FormDB.ListView1.Items.Count-1].SubItems[7] <> '' then
   if AnsiPos('@',FormDB.ListView1.Items.Item[FormDB.ListView1.Items.Count-1].SubItems[7]) > 0 then
     FormDB.ListView1.Items.Item[FormDB.ListView1.Items.Count-1].SubItems[7] := '@'+
     FormatFloat('00000000',StrToIntDef(Copy(FormDB.ListView1.Items.Item[FormDB.ListView1.Items.Count-1].SubItems[7],2,100),0)) else
     FormDB.ListView1.Items.Item[FormDB.ListView1.Items.Count-1].SubItems[7] :=
     FormatFloat('00000000',StrToIntDef(FormDB.ListView1.Items.Item[FormDB.ListView1.Items.Count-1].SubItems[7],0));
  end;
     
  ok := False;
  lista.Clear;
 end;
  FreeAndNil(lista);
 FormDB.ListView1.Items.EndUpdate;
 FormDB.Label2.Caption := IntToStr(FormDB.ListView1.Items.Count) + ' item(ns) apresentado(s)';
end;

procedure TFormDB.btnExportClick(Sender: TObject);
var
  i, x: Integer;
  csv: TStringList;
  temp: string;
begin
 if dlgSave1.Execute then
 if ExtractFileName(dlgSave1.FileName) <> '' then
 begin
  csv := TStringList.Create;
  csv.Clear;
  temp := '';
  for i:= 0 to ListView1.Columns.Count-1 do
   temp := temp + ListView1.Columns.Items[i].Caption + ';';
  csv.Add(temp);
  temp := '';
  for i:= 0 to ListView1.Items.Count-1 do
  begin
    temp := ListView1.Items.Item[i].Caption + ';';
    for x:= 0 to ListView1.Columns.Count-2 do
    temp := temp + ListView1.Items.Item[i].SubItems.Strings[x] + ';';
    csv.Add(temp);
    temp := '';
  end;
  csv.SaveToFile(dlgSave1.FileName);
  FreeAndNil(csv);
 end;
end;

procedure DropListNPC(s1,s2,s3: String);
  procedure separa(Texto: String; var lista: TStringList);
  begin
   Texto := Trim(Texto);
   if Copy(Texto,Length(Texto),Length(Texto)) <> '/' then
   Texto := Texto + '/';
   while AnsiPos('/',Texto) > 0 do
   begin
    lista.Add(Copy(Texto,0,AnsiPos('/',Texto)-1));
    Delete(Texto,1,AnsiPos('/',Texto));
   end;
  end;
var
  i: Integer;
  lista: TStringList;
  snpc: string;
begin
  lista := TStringList.Create;
  lista.BeginUpdate;
      FormDB.lv1.Items.BeginUpdate;
      FormDB.lv2.Items.BeginUpdate;
      FormDB.lv3.Items.BeginUpdate;
      FormDB.lv1.Clear;
          FormDB.lv2.Clear;
              FormDB.lv3.Clear;
  separa(Trim(s1),lista);
  for i:= 0 to lista.Count-1 do
  if lista.Strings[i] <> '' then
  begin
    if Copy(lista.Strings[i],0,1) = '@' then
     snpc := Copy(lista.Strings[i],2,AnsiPos(' (',lista.Strings[i])-1) else
     snpc := Copy(lista.Strings[i],0,AnsiPos(' (',lista.Strings[i]));
    FormDB.lv1.Items.Add.Caption := lista.Strings[i];
    FormDB.lv1.Items.Item[FormDB.lv1.Items.Count-1].ImageIndex := AnsiIndexText(Trim(snpc),NPC);
  end;
  lista.Clear;
  separa(s2,lista);
  for i:= 0 to lista.Count-1 do
  if lista.Strings[i] <> '' then
  begin
    if Copy(lista.Strings[i],0,1) = '@' then
     snpc := Copy(lista.Strings[i],2,AnsiPos(' (',lista.Strings[i])-1) else
     snpc := Copy(lista.Strings[i],0,AnsiPos(' (',lista.Strings[i]));
    FormDB.lv2.Items.Add.Caption := lista.Strings[i];
    FormDB.lv2.Items.Item[FormDB.lv2.Items.Count-1].ImageIndex := AnsiIndexText(Trim(snpc),NPC);
  end;
  lista.Clear;
  separa(s3,lista);
  for i:= 0 to lista.Count-1 do
  if lista.Strings[i] <> '' then  
  begin
    if Copy(lista.Strings[i],0,1) = '@' then
     snpc := Copy(lista.Strings[i],2,AnsiPos(' (',lista.Strings[i])-1) else
     snpc := Copy(lista.Strings[i],0,AnsiPos(' (',lista.Strings[i]));
    FormDB.lv3.Items.Add.Caption := lista.Strings[i];
    FormDB.lv3.Items.Item[FormDB.lv3.Items.Count-1].ImageIndex := AnsiIndexText(Trim(snpc),NPC);
  end;
      FormDB.lv1.Items.EndUpdate;
      FormDB.lv2.Items.EndUpdate;
      FormDB.lv3.Items.EndUpdate;
  lista.EndUpdate;
  lista.Clear;
  FreeAndNil(lista);
end;

procedure TFormDB.ListView1Click(Sender: TObject);
var
  index,i, ID: Integer;
begin
  index := ListView1.ItemIndex;
 if FormDB.Width > btnMaxMin.Left+30 then
 if (index >= 0) and (index < ListView1.Items.Count) then
 begin
    //ListView1.Items.BeginUpdate;
    if (sCheckShowIMG.Checked) then
    begin
      ID := StrToIntDef(ListView1.Items.Item[index].Caption, 0);
      Form1.visualizarImagem(ID, imgShow);
      imgShow.Refresh;
    end;

    imgType.Picture := nil;
    lblNum.Caption := FormatFloat('000',StrToIntDef(ListView1.Items.Item[index].Caption,0));
    lbEstrelas.Caption := StringOfChar(star,StrToIntDef(ListView1.Items.Item[index].SubItems.Strings[4],0));
    lbNome.Caption := 'Nome: '+ListView1.Items.Item[index].SubItems.Strings[0];
    lbTipoCard.Caption := ListView1.Items.Item[index].SubItems.Strings[1];
    lbTipo.Caption := 'Tipo: '+ListView1.Items.Item[index].SubItems.Strings[2];
    if ListView1.Items.Item[index].SubItems.Strings[2] = '' then
    begin
     case AnsiIndexText(ListView1.Items.Item[index].SubItems.Strings[1],['Equip','Field','Magic','Ritual','Trap']) of
       0..4: ilImgCType.GetIcon(
             AnsiIndexText(ListView1.Items.Item[index].SubItems.Strings[1],T_Card),
             imgType.Picture.Icon);
      end;
    end else
     ilImgType.GetIcon(
     AnsiIndexText(ListView1.Items.Item[index].SubItems.Strings[2],Tipo),
     imgType.Picture.Icon);


    lbPass.Caption := Trim(ListView1.Items.Item[index].SubItems.Strings[7]);
    lbCusto.Caption := ListView1.Items.Item[index].SubItems.Strings[8];
    lbATK.Caption := 'ATK: '+ListView1.Items.Item[index].SubItems.Strings[5];
    lbDEF.Caption := 'DEF: '+ListView1.Items.Item[index].SubItems.Strings[6];
    lbSymbol.Caption :=
    Symbol[
    AnsiIndexText(ListView1.Items.Item[index].SubItems.Strings[3],Simbolo)+1
    ] + #13 +
    Symbol[
    AnsiIndexText(ListView1.Items.Item[index].SubItems.Strings[9],Simbolo)+1
    ];
    DropListNPC(ListView1.Items.Item[index].SubItems.Strings[10],
             ListView1.Items.Item[index].SubItems.Strings[11],
             ListView1.Items.Item[index].SubItems.Strings[12]);

    sLabelVits.Caption := '';
    for i:=0 to Length(DropList)-1 do
    if DropList[i].Duel > 0 then
    if DropList[i].ID = StrToIntDef(ListView1.Items.Item[index].Caption,0) then
    begin
      sLabelVits.Caption := 'Ap�s: '+IntToStr(DropList[i].Duel)+' vit�rias.';
    end;
   //ListView1.Items.EndUpdate;
 end;
end;

procedure TFormDB.btnExportDropClick(Sender: TObject);
var
  i, x: Integer;
  csv: TStringList;
  temp: string;
begin
 if dlgSave1.Execute then
 if ExtractFileName(dlgSave1.FileName) <> '' then
 begin
  csv := TStringList.Create;
  csv.Clear;
  temp := '';
  temp := 'ID;SA Pow;SA Tec; BCD Pow/Tec;';
  csv.Add(temp);
  temp := '';
  for i:= 0 to ListView1.Items.Count-1 do
  begin
    temp := ListView1.Items.Item[i].Caption + ';';
    for x:= ListView1.Columns.Count-4 to ListView1.Columns.Count-2 do
    temp := temp + ListView1.Items.Item[i].SubItems.Strings[x] + ';';
    csv.Add(temp);
    temp := '';
  end;
  csv.SaveToFile(dlgSave1.FileName);
  FreeAndNil(csv);
 end;
end;

procedure TFormDB.ListView1CustomDrawItem(Sender: TCustomListView;
  Item: TListItem; State: TCustomDrawState; var DefaultDraw: Boolean);
begin
  if Item.SubItems.Count >= 12 then
    if (AnsiPos('@',item.SubItems[7]) > 0) or
       (AnsiPos('@',item.SubItems[8]) > 0) or
       (AnsiPos('@',item.SubItems[10]) > 0) or
       (AnsiPos('@',item.SubItems[11]) > 0) or
       (AnsiPos('@',item.SubItems[12]) > 0) then
    Sender.Canvas.Font.Color := clRed;
end;

procedure TFormDB.ListView1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if (Key = VK_UP) or (Key = VK_DOWN) then
 ListView1.OnClick(Sender);
end;

procedure TFormDB.Edit1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if key = VK_RETURN then
    sBtnSearch.Click;
end;

procedure TFormDB.lv1CustomDrawItem(Sender: TCustomListView;
  Item: TListItem; State: TCustomDrawState; var DefaultDraw: Boolean);
begin
   if Copy(Item.Caption,0,1) = '@' then
    Sender.Canvas.Font.Color := clRed;
end;

procedure TFormDB.sCheckShowIMGClick(Sender: TObject);
begin
 if (not sCheckShowIMG.Checked) then
   imgShow.Picture := imgBKP.Picture;
 Config.wDBImgShow := sCheckShowIMG.Checked;
end;

procedure TFormDB.FormShow(Sender: TObject);
begin
 Config.wDBMax := not Config.wDBMax;
 btnMaxMin.Click;
 sCheckShowIMG.Checked := Config.wDBImgShow;
 if not sCheckShowIMG.Checked then
 imgShow.Picture := imgBKP.Picture;
 btnExport.Enabled := Config.IsDebug77;
 btnExportDrop.Enabled := Config.IsDebug77;
 lbSymbol.Font.Name := 'YuGiOh';
 lbEstrelas.Font.Name := 'YuGiOh';
 lbEstrelas.Font.Charset := ANSI_CHARSET;
 ListView1.DoubleBuffered := True;
end;

procedure TFormDB.sBtnSearchClick(Sender: TObject);
begin
 if RadioGroup1.ItemIndex in[1,6,7,8,10] then
 if StrToIntDef(Trim(Edit1.Text),-999999999) = -999999999 then
 begin
   ShowMessage('Valor Inv�lido Campo 1!'+#13+'Devera conter somente n�meros.');
   Edit1.SelectAll;
   Edit1.SetFocus;
   Exit;
 end else
 if Trim(Edit2.Text) <> '' then
 if StrToIntDef(Trim(Edit2.Text),-999999999) = -999999999 then
 begin
   ShowMessage('Valor Inv�lido Campo 2!'+#13+'Devera conter somente n�meros.');
   Edit2.SelectAll;
   Edit2.SetFocus;
   Exit;
 end;

  Procurar(RadioGroup1.ItemIndex,Trim(Edit1.Text),Trim(Edit2.Text));
end;

procedure TFormDB.btnMaxMinClick(Sender: TObject);
begin
 if Config.wDBMax then
 begin
    //FormDB.Width := FormDB.Width - PanelDetails.Width;
    btnMaxMin.Caption := 'M'+#13+'O'+#13+'S'+#13+'T'+#13+'R'+#13+'A'+#13+'R'+#13+' '+#13+'D'+#13+'E'+#13+'T'+#13+'A'+#13+'L'+#13+'H'+#13+'E'+#13+'S';
    Config.wDBMax := False;
    PanelDetails.Width := 0;
 end else
 begin
    PanelDetails.Width := 465;
    //FormDB.Width := FormDB.Width + PanelDetails.Width;
    btnMaxMin.Caption := 'O'+#13+'C'+#13+'U'+#13+'L'+#13+'T'+#13+'A'+#13+'R'+#13+' '+#13+'D'+#13+'E'+#13+'T'+#13+'A'+#13+'L'+#13+'H'+#13+'E'+#13+'S';
    ListView1.OnClick(ListView1);
    Config.wDBMax := True;
 end;
 FormDB.Repaint;
end;

procedure TFormDB.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_ESCAPE then
     Close;
end;

procedure TFormDB.sSpeedButtonPassClick(Sender: TObject);
var
  hProcess: THandle;
  pass: string;
  passNumber,IsViraCard: Byte;
  IDCard: Word;
  i: Integer;
  temp: Cardinal;
begin
  if Form1.TIProcess.Enabled then
  begin
   hProcess := OpenProcess(PROCESS_ALL_ACCESS,FALSE,PID);
   ReadProcessMemory(hProcess,ptr(Form1.GetEmule(AD_PASS_SHOW_CARD)), Addr(IsViraCard),1,temp);
   if Form1.sStatusBar1.Panels.Items[0].Text = 'YuGiOh PassWord' then
   begin
     SetForegroundWindow(FindWindow('EPSX',nil));
     if (IsViraCard = 0) and (StrToIntDef(lblNum.Caption,0) > 0) then
     begin
       if lbPass.Caption[1] = '@' then
        pass := FormatFloat('00000000',StrToIntDef(copy(lbPass.Caption,2,8),0)) else
        pass := FormatFloat('00000000',StrToIntDef(lbPass.Caption,0));
       for i:= 1 to 8 do
       begin
        passNumber := StrToInt(pass[i]);
        WriteProcessMemory(hProcess,ptr(Form1.GetEmule(AD_PASS_NUM[i])), Addr(PassNumber),1,temp);
       end;
       IDCard := StrToInt(lblNum.Caption);
       WriteProcessMemory(hProcess,ptr(Form1.GetEmule(AD_PASS)), Addr(IDCard),2,temp);
       Sleep(600);
       IsViraCard := 16;
       WriteProcessMemory(hProcess,ptr(Form1.GetEmule(AD_END_MOVE)), Addr(IsViraCard),1,temp);
       Sleep(600);
       IsViraCard := 64;
       WriteProcessMemory(hProcess,ptr(Form1.GetEmule(AD_END_MOVE)), Addr(IsViraCard),1,temp);
       Sleep(100);
       IsViraCard := 1;
       WriteProcessMemory(hProcess,ptr(Form1.GetEmule(AD_PASS_SHOW_CARD)), Addr(IsViraCard),1,temp);
      end else
     if (StrToIntDef(lblNum.Caption,0) > 0) then
      begin
       IsViraCard := 64;
       WriteProcessMemory(hProcess,ptr(Form1.GetEmule(AD_END_MOVE)), Addr(IsViraCard),1,temp);
       Sleep(50);
       WriteProcessMemory(hProcess,ptr(Form1.GetEmule(AD_END_MOVE)), Addr(IsViraCard),1,temp);
       Sleep(50);
       WriteProcessMemory(hProcess,ptr(Form1.GetEmule(AD_END_MOVE)+11), Addr(IsViraCard),1,temp);
       Sleep(50);
       sSpeedButtonPass.Click;
      end else
       ShowMessage('PassWord Inv�lido!');
   end else
    ShowMessage('Para carregar o PassWord, voc� deve estar na janela do Password no Jogo!');
  end else
    ShowMessage('O M�dulo n�o est� ativo!');
end;

end.
