unit Unit3;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, sScrollBox, StdCtrls, sMemo, sGroupBox, sLabel,
  dxGDIPlusClasses, ExtCtrls, ShellAPI;

type
  TForm3 = class(TForm)
    sScrollBox1: TsScrollBox;
    sGroupBox1: TsGroupBox;
    sGroupBox2: TsGroupBox;
    sGroupBox3: TsGroupBox;
    sLabel1: TsLabel;
    img1: TImage;
    sLabel2: TsLabel;
    sLabel3: TsLabel;
    img2: TImage;
    sLabel4: TsLabel;
    sLabel5: TsLabel;
    sLabel6: TsLabel;
    img3: TImage;
    sGroupBox4: TsGroupBox;
    img4: TImage;
    sLabel7: TsLabel;
    img5: TImage;
    sLabel8: TsLabel;
    sLabel9: TsLabel;
    img6: TImage;
    sLabel10: TsLabel;
    sLabel11: TsLabel;
    img7: TImage;
    sLabel12: TsLabel;
    sLabel13: TsLabel;
    img8: TImage;
    img9: TImage;
    img10: TImage;
    sLabel14: TsLabel;
    img11: TImage;
    sLabel15: TsLabel;
    sLabel16: TsLabel;
    sLabel17: TsLabel;
    sGroupBox5: TsGroupBox;
    sLabel18: TsLabel;
    img12: TImage;
    img13: TImage;
    img14: TImage;
    sLabel19: TsLabel;
    sLabel20: TsLabel;
    sLabel21: TsLabel;
    sLabel22: TsLabel;
    sLabel23: TsLabel;
    sGroupBox6: TsGroupBox;
    sLabel24: TsLabel;
    sWebLabel1: TsWebLabel;
    sWebLabel2: TsWebLabel;
    sWebLabel3: TsWebLabel;
    sWebLabel4: TsWebLabel;
    img15: TImage;
    sLabel25: TsLabel;
    img16: TImage;
    sLabel26: TsLabel;
    sLabel27: TsLabel;
    sLabel28: TsLabel;
    sLabel29: TsLabel;
    sWebLabel5: TsWebLabel;
    sWebLabel6: TsWebLabel;
    sWebLabel7: TsWebLabel;
    sLabel30: TsLabel;
    procedure FormMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure FormMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sWebLabel5Click(Sender: TObject);
    procedure sWebLabel6Click(Sender: TObject);
    procedure sWebLabel7Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form3: TForm3;

implementation

uses Unit1;

{$R *.dfm}

procedure TForm3.FormMouseWheelDown(Sender: TObject; Shift: TShiftState;
  MousePos: TPoint; var Handled: Boolean);
begin
  with sScrollBox1.VertScrollBar do
   begin
    if (Position <= (Range - Increment)) then
     Position := Position + Increment else
      Position := Range - Increment;
   end;
end;

procedure TForm3.FormMouseWheelUp(Sender: TObject; Shift: TShiftState;
  MousePos: TPoint; var Handled: Boolean);
begin
 with sScrollBox1.VertScrollBar do
 begin
  if (Position >= Increment) then
   Position := Position - Increment else
    Position := 0;
 end;
end;

procedure TForm3.FormCreate(Sender: TObject);
begin
 DoubleBuffered := True;
 sScrollBox1.VertScrollBar.Position := 0;
end;

procedure TForm3.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_ESCAPE then
     Close;
end;

procedure TForm3.sWebLabel5Click(Sender: TObject);
begin
 TForm1.reproduzirAudio();
end;

procedure TForm3.sWebLabel6Click(Sender: TObject);
begin
 TForm1.reproduzirAudio(1);
end;

function ExtractWindowsDir : String;
var Buffer : Array[0..144] of Char;
Begin
 GetWindowsDirectory(Buffer,144);
 Result := IncludeTrailingBackSlash(StrPas(Buffer));
End;

procedure VerificarFontes;
var
 Stream: TResourceStream;
begin
  Stream := TResourceStream.Create(hInstance,'FONTYG', RT_RCDATA);
  CreateDir(ExtractFileDir(ParamStr(0))+'\Fontes');
  Stream.SaveToFile(ExtractFileDir(ParamStr(0))+'\Fontes\YugiOh.ttf');
  Stream.Free;
  
  if Form1.UnderWine then
  begin
    Application.MessageBox(PChar('Usu�rio Linux, instale a fonte que foi gerado dentro da pasta do aplicativo na subpasta: "Fontes", abra o arquivo YugiOh.ttf com seu visualizador de fontes e instale-o!'),'ATEN��O!',0);
    ShellExecute(Application.Handle,'open',PChar(ExtractFileDir(ParamStr(0))+'\Fontes\'),nil,nil,SW_SHOWNORMAL);
  end else
  begin
    Application.MessageBox(PChar('Infelizmente o aplicativo n�o consegue criar automaticamente os arquivos fontes no diret�rio! Por gentileza, insira manualmente os arquivos...'+#13+'ATEN��O: Ser� aberto uma pasta com a fonte, copie e cole na pasta: '+ExtractWindowsDir+'Fonts\'),'ATEN��O!',0);
    ShellExecute(Application.Handle,'open',PChar(ExtractFileDir(ParamStr(0))+'\Fontes\'),nil,nil,SW_SHOWNORMAL);
    ShellExecute(Application.Handle,'open',PChar(ExtractWindowsDir+'Fonts\'),nil,nil,SW_SHOWNORMAL);
  end;
end;

procedure TForm3.sWebLabel7Click(Sender: TObject);
begin
  VerificarFontes;
end;

end.
